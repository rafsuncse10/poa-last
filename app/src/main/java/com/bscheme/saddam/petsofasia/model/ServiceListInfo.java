package com.bscheme.saddam.petsofasia.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Bscheme on 2/4/16.
 */
public class ServiceListInfo implements Parcelable {

    public String title;
    public int id;
    public String imgLink;
    public ArrayList<String> imageList = new ArrayList<>();
    public ServiceListInfo(){

    }
    protected ServiceListInfo(Parcel in) {
        id = in.readInt();
        title = in.readString();
        imgLink = in.readString();
        in.readStringList(imageList);
    }

    public static final Creator<ServiceListInfo> CREATOR = new Creator<ServiceListInfo>() {
        @Override
        public ServiceListInfo createFromParcel(Parcel in) {
            return new ServiceListInfo(in);
        }

        @Override
        public ServiceListInfo[] newArray(int size) {
            return new ServiceListInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(imgLink);
        dest.writeStringList(imageList);
    }
}
