package com.bscheme.saddam.petsofasia.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.activity.nav_drawer.MainActivity;
import com.bscheme.saddam.petsofasia.utils.CheckConnectivity;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.workarounds.typography.EditText;
import in.workarounds.typography.TextView;

public class LogIn extends AppCompatActivity {

    SweetAlertDialog mDialog;
    EditText edt_email,edtPassword;
    LinearLayout btnLogin,loginFb;

    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Facebook sdk initialize*/
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_log_in);

        TextView newMember = (TextView) findViewById(R.id.notAMember);
        newMember.setText(Html.fromHtml("Not a <u>Member?</u>"));

        TextView forgotPassword = (TextView) findViewById(R.id.forgotYourPassword);
        forgotPassword.setText(Html.fromHtml("Forgot your <u>Password?</u>"));

        edt_email = (EditText) findViewById(R.id.emailAddress);
        edtPassword = (EditText) findViewById(R.id.password);
        btnLogin = (LinearLayout) findViewById(R.id.logIn);
        loginFb = (LinearLayout) findViewById(R.id.logInUsingFacebook);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edt_email.getText().toString()) || TextUtils.isEmpty(edtPassword.getText().toString())) {
                    Toast.makeText(LogIn.this, "Please fill all field", Toast.LENGTH_SHORT).show();
                } else {
                    CheckConnectivity connectivity=new CheckConnectivity(LogIn.this);

                    if(connectivity.isConnected())
                        new getLoginTask().execute(edt_email.getText().toString(), edtPassword.getText().toString(),"app");
                    else Toast.makeText(LogIn.this,"Check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });

        newMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogIn.this, CreateAccount.class);
                startActivity(i);
            }
        });
        ((TextView)findViewById(R.id.term_condition)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogIn.this, TermsConditionActivity.class);
                startActivity(i);
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LogIn.this, ForgotPassword.class);
                startActivity(i);
            }
        });

        loginFb.setOnClickListener(fbClickListener);

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, first_name, last_name,name, email, birthday, location");
                        /* make the API call */
                        new GraphRequest(
                                AccessToken.getCurrentAccessToken(),
                                "/" + loginResult.getAccessToken().getUserId(),
                                parameters,
                                HttpMethod.GET,
                                new GraphRequest.Callback() {
                                    public void onCompleted(GraphResponse response) {
                                        /* handle the result */
                                        Log.i("graph", "" + response.getJSONObject());
                                        JSONObject object = response.getJSONObject();


                                        String name,email = null,id;
                                        try {
                                            if (object.has("first_name"))

                                            if (object.has("last_name"))
                                            if (object.has("email"))
                                                email =  object.getString("email");
                                            if (object.has("name"))
                                                name = object.getString("name");
                                            CheckConnectivity connectivity=new CheckConnectivity(LogIn.this);

                                            if(connectivity.isConnected())
                                                new getLoginTask().execute(email, "","fb");
                                            else Toast.makeText(LogIn.this,"Check your internet connection !",Toast.LENGTH_SHORT).show();

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }).executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LogIn.this, "User declined ", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LogIn.this, "" + exception.toString(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private View.OnClickListener fbClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginManager.getInstance().logInWithReadPermissions(LogIn.this, Arrays.asList("public_profile", "email"));

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(true);
            getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }

    void closeDialog(){
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
        }
        ((FullScreenDialog)prev).dismiss();
    }
    private class getLoginTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = new SweetAlertDialog(LogIn.this, SweetAlertDialog.PROGRESS_WITHOUT_TEXT_TYPE);
            mDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.primary_color_dark));
            mDialog.setTitleText("");
            mDialog.setCancelable(true);
//            mDialog.show();

            showDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                ConnectionHelper ch = new ConnectionHelper();
                ch.createConnection(Urls.URL_COMMON, "POST");

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("action", "user_login")
                        .appendQueryParameter("user_id", params[0])
                        .appendQueryParameter("password", params[1])
                        .appendQueryParameter("gcm_id", params[1])
                        .appendQueryParameter("source", params[2]);
                ch.addData(builder);
                return ch.getResponse();
            }catch (Exception e){
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s)) {
                Log.i("res", "" + s);

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {

                        closeDialog();
                        Toast.makeText(getApplicationContext(), "" + jObj.getString("status"), Toast.LENGTH_SHORT).show();
                    } else {
                        if (jObj.has("profile_image") || jObj.has("profile_image"))
                        SharedDataSaveLoad.save(LogIn.this, getResources().getString(R.string.shared_pref_user_id), jObj.getString("user_id"));
                        SharedDataSaveLoad.save(LogIn.this, getResources().getString(R.string.shared_pref_user_email), jObj.getString("email"));
                        SharedDataSaveLoad.save(LogIn.this, getResources().getString(R.string.shared_pref_user_name), jObj.getString("name"));
                        if (jObj.has("profile_image"))
                        SharedDataSaveLoad.save(LogIn.this, getResources().getString(R.string.shared_pref_user_image), jObj.getString("profile_image"));

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        closeDialog();
                        setResult(1);
                        LogIn.this.finish();
                    }
                } catch (JSONException e) {
                    closeDialog();
                    Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }
    private class getLoginFBTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = new SweetAlertDialog(LogIn.this, SweetAlertDialog.PROGRESS_WITHOUT_TEXT_TYPE);
            mDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.primary_color_dark));
            mDialog.setTitleText("");
            mDialog.setCancelable(true);
//            mDialog.show();

            showDialog();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                ConnectionHelper ch = new ConnectionHelper();
                ch.createConnection(Urls.URL_COMMON, "POST");

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("action", "user_login")
                        .appendQueryParameter("user_id", params[0])
                        .appendQueryParameter("password", params[1])
                        .appendQueryParameter("gcm_id", params[1])
                        .appendQueryParameter("source", params[2]);
                ch.addData(builder);
                return ch.getResponse();
            }catch (Exception e){
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s)) {
                Log.i("res", "" + s);

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {

                        closeDialog();
                        Toast.makeText(getApplicationContext(), "" + jObj.getString("status"), Toast.LENGTH_SHORT).show();
                    } else {
                        SharedDataSaveLoad.save(LogIn.this, getResources().getString(R.string.shared_pref_user_id), jObj.getString("user_id"));
                        SharedDataSaveLoad.save(LogIn.this, getResources().getString(R.string.shared_pref_user_email), jObj.getString("user_email"));
                        SharedDataSaveLoad.save(LogIn.this, getResources().getString(R.string.shared_pref_user_name), jObj.getString("full_name"));
                        if (jObj.has("profile_image"))
                            SharedDataSaveLoad.save(LogIn.this, getResources().getString(R.string.shared_pref_user_image), jObj.getString("profile_image"));

                        closeDialog();

                        startActivity(new Intent(LogIn.this, MainActivity.class));
                    }
                } catch (JSONException e) {
                    closeDialog();
                    Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }

}
