package com.bscheme.saddam.petsofasia.fragment.fragment_left_menu;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.adapter.FavouriteServiceProviderAdapter;
import com.bscheme.saddam.petsofasia.model.ServiceProviderListInfo;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.DividerItemDecoration;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */

public class ServicesFavouriteFragment extends Fragment {

    List<ServiceProviderListInfo> mServiceProviderListInfos;
    RecyclerView mRecyclerView;
    FavouriteServiceProviderAdapter mAdapter;

    public ServicesFavouriteFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_services_favourite, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_fav_servProvider);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(gridLayoutManager);
        DividerItemDecoration divider = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        mRecyclerView.addItemDecoration(divider);

        mServiceProviderListInfos = new ArrayList<>();
        mAdapter = new FavouriteServiceProviderAdapter(getActivity(),mServiceProviderListInfos);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new geServiceProviderListTask().execute();
    }

    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(false);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }
    void closeDialog(){
        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getActivity().getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
//            prev.dismiss();
            Log.i("frag diag", "close");
        }
    }
    private class geServiceProviderListTask extends AsyncTask<Integer,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showDialog();

        }

        @Override
        protected String doInBackground(Integer... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "user_favorite_service_list")
                    .appendQueryParameter("user_id", ""+ SharedDataSaveLoad.load(getActivity(),getActivity().getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            closeDialog();
            ServiceProviderListInfo mServiceProviderInfo;
            if (!TextUtils.isEmpty(s)) {

                Log.i("response serv feb" ,s);
                try {

                    JSONObject jObj = new JSONObject(s);

                    if (jObj.getInt("success") == 0){
                        
                    }else {
                    JSONArray jsonArray = jObj.getJSONArray("service_list");

                    for (int i=0; i< jsonArray.length(); i++) {

                        mServiceProviderInfo = new ServiceProviderListInfo();

                        mServiceProviderInfo.id = jsonArray.getJSONObject(i).getInt("id");
                        mServiceProviderInfo.title = jsonArray.getJSONObject(i).getString("title");
//                        mServiceProviderInfo.about = jsonArray.getJSONObject(i).getString("about");
                        mServiceProviderInfo.imgLink = jsonArray.getJSONObject(i).getString("featured_image");
//                            mPetIndividualInfo.location = jsonArray.getJSONObject(i).getString("location");
//                            mPetIndividualInfo.shelter = jsonArray.getJSONObject(i).getString("shelter");
//                            mPetIndividualInfo.type = jsonArray.getJSONObject(i).getString("type");
//                            mPetIndividualInfo.gender = jsonArray.getJSONObject(i).getString("gender");
//                            mPetIndividualInfo.age_range = jsonArray.getJSONObject(i).getString("age_range");
//                            mPetIndividualInfo.size_type = jsonArray.getJSONObject(i).getString("size_type");
//                            mPetIndividualInfo.breed_type = jsonArray.getJSONObject(i).getString("breed_type");
//                            mPetIndividualInfo.vaccinated = jsonArray.getJSONObject(i).getString("vaccinated");
//                        mServiceProviderInfo.contact_email = jsonArray.getJSONObject(i).getString("contact_email");
//                        mServiceProviderInfo.contact_address = jsonArray.getJSONObject(i).getString("contact_address");
//                        mServiceProviderInfo.contact_phone = jsonArray.getJSONObject(i).getString("contact_phone");
//                        mServiceProviderInfo.contact_web = jsonArray.getJSONObject(i).getString("contact_web");
//                        mServiceProviderInfo.favourite = jsonArray.getJSONObject(i).getInt("favourite");

                        JSONArray jsonServCat = jsonArray.getJSONObject(i).getJSONArray("service_cat");
                        for (int k = 0; k < jsonServCat.length(); k++) {

                            mServiceProviderInfo.serviceList.add(jsonServCat.getJSONObject(k).getString("service_cat_name"));

                        }

                        mServiceProviderListInfos.add(mServiceProviderInfo);

                        mAdapter.notifyDataSetChanged();
                    }
                    }

                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }
}
