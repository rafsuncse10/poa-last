package com.bscheme.saddam.petsofasia.activity;

import android.app.Application;

import com.facebook.FacebookSdk;

import in.workarounds.typography.FontLoader;

/**
 * Created by Bscheme on 2/3/16.
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FontLoader.setDefaultFont("BloggerSans", "Regular");
        /*Facebook sdk initialize*/
//        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
