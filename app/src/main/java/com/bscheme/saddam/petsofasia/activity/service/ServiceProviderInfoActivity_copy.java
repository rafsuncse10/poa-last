package com.bscheme.saddam.petsofasia.activity.service;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.SwipeAdapterPetIndividual;
import com.bscheme.saddam.petsofasia.activity.ServiceIndividualView;
import com.bscheme.saddam.petsofasia.model.PetIndividualInfo;
import com.bscheme.saddam.petsofasia.model.ServiceProviderInfo;
import com.bscheme.saddam.petsofasia.fragment.service_provider.ServiceAboutFragment;
import com.bscheme.saddam.petsofasia.fragment.service_provider.ServiceContactFragment;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.CustomPager;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.workarounds.typography.TextView;
import me.relex.circleindicator.CircleIndicator;

public class ServiceProviderInfoActivity_copy extends AppCompatActivity {

    TextView txtHeaders;
    ViewPager viewPagerImage, viewPagerTab;
    MyPagerAdapter myPagerAdapter;

    TabLayout pagerSlidingTabStrip;
    CircleIndicator indicator;

    SwipeAdapterPetIndividual swipeAdapterImage;
    ServiceProviderInfo mPetIndividualInfo;

    ImageView ivFav;

    /////////////
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_individual_copy);

        txtHeaders = (TextView) findViewById(R.id.txtHeaders);
        viewPagerImage = (ViewPager) findViewById(R.id.view_pager_image);
        indicator = (CircleIndicator) findViewById(R.id.indicator_default);
        viewPagerTab = (ViewPager) findViewById(R.id.view_pager_two);
        ivFav = (ImageView) findViewById(R.id.iv_favourite);

        pagerSlidingTabStrip = (TabLayout) findViewById(R.id.tabs);

//        setupTablayout();
        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());

        new getServProvoderInfoTask().execute("20");// get provider info task

        /*image slider*/

        /*
        swipeAdapter = new SwipeAdapterCategoryIndividual(getSupportFragmentManager());
        viewPagerImage.setAdapter(swipeAdapter);

        indicator.setViewPager(viewPagerImage);*/

        viewPagerTab.setAdapter(myPagerAdapter);

        //PagerSlidingTabStrip pagerSlidingTabStripTwo = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        /*viewPagerImage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int mScrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
//                viewPagerTab.scrollTo(viewPagerImage.getScrollX(), viewPagerTab.getScrollY());
            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(final int state) {
                mScrollState = state;
                if (state == ViewPager.SCROLL_STATE_IDLE) {
//                    viewPagerTab.setCurrentItem(viewPagerImage.getCurrentItem(), false);
                }
            }
        });*/

        viewPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int mScrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
//                viewPagerImage.scrollTo(viewPagerTab.getScrollX(), viewPagerImage.getScrollY());
            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(final int state) {
                mScrollState = state;
                if (state == ViewPager.SCROLL_STATE_IDLE) {
//                    viewPagerImage.setCurrentItem(viewPagerTab.getCurrentItem(), false);
                }
            }
        });

        pagerSlidingTabStrip.setupWithViewPager(viewPagerTab);

        ivFav.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mPetIndividualInfo != null) {
                    if (mPetIndividualInfo.favourite == 1) {
                        Toast.makeText(getApplicationContext(), "Already added in favourite list !!!", Toast.LENGTH_SHORT).show();
                        ivFav.setImageResource(R.drawable.heartcolor);
                    } else {
                        new setFavouriteTask().execute(mPetIndividualInfo.id);

                    }
                }

            }

        });
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        private int mCurrentPosition = -1;
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                CustomPager pager = (CustomPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position;
                    pager.measureCurrentView(fragment.getView());
                }
            }
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0: return ServiceAboutFragment.newInstance(mPetIndividualInfo);
                case 1: return ServiceContactFragment.newInstance(mPetIndividualInfo);
                case 2: return new Fragment();
                case 3: return new Fragment();
                default: return ServiceAboutFragment.newInstance(mPetIndividualInfo);
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) { // Set the tab text
            if (position == 0) {
                return "ABOUT";
            }
            if (position == 1) {
                return "CONTACT";
            }
            if (position == 2) {
                return "LOCATE";
            }
            if (position == 3) {
                return "MESSAGE";
            }
            return getPageTitle(position);
        }

    }

    public void onClickEnlarge(View view) {

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager_image);

        Intent i = new Intent(this, ServiceIndividualView.class).putExtra("positionTwo", viewPager.getCurrentItem());
        startActivity(i);

    }

    public void onClickBack(View view) {
        finish();
    }
    private void updateUI(ServiceProviderInfo serviceProviderInfo) {

        txtHeaders.setText(serviceProviderInfo.title);

        if(serviceProviderInfo.favourite == 1) {

            ivFav.setImageResource(R.drawable.heartcolor);

        } else if(serviceProviderInfo.favourite == 0) {

            ivFav.setImageResource(R.drawable.heart);

        }
    }


    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(false);
            /*getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();*/

             fragment.show(getSupportFragmentManager().beginTransaction(), "dialog");
        }

    }
    void closeDialog(){
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
//            getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
                    ((FullScreenDialog)prev).dismiss();

        }
    }
    private class getServProvoderInfoTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "service_detail_by_id")
                    .appendQueryParameter("post_id",""+params[0])
                    .appendQueryParameter("user_id",""+ SharedDataSaveLoad.load(ServiceProviderInfoActivity_copy.this, getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s))

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {

                        closeDialog();
                        Toast.makeText(getApplicationContext(), "Request not completed", Toast.LENGTH_SHORT).show();
                    } else {

                        closeDialog();
                        JSONArray jsonArray = jObj.getJSONArray("pet_detail");

                        for (int i=0; i< jsonArray.length(); i++){

                            mPetIndividualInfo = new ServiceProviderInfo();

                            mPetIndividualInfo.id = jsonArray.getJSONObject(i).getInt("id");
                            mPetIndividualInfo.title = jsonArray.getJSONObject(i).getString("title");
                            mPetIndividualInfo.about = jsonArray.getJSONObject(i).getString("about");
                            mPetIndividualInfo.featured_image = jsonArray.getJSONObject(i).getString("featured_image");
//                            mSProviderInfo.location = jsonArray.getJSONObject(i).getString("location");
//                            mSProviderInfo.shelter = jsonArray.getJSONObject(i).getString("shelter");
//                            mSProviderInfo.type = jsonArray.getJSONObject(i).getString("type");
//                            mSProviderInfo.gender = jsonArray.getJSONObject(i).getString("gender");
//                            mSProviderInfo.age_range = jsonArray.getJSONObject(i).getString("age_range");
//                            mSProviderInfo.size_type = jsonArray.getJSONObject(i).getString("size_type");
//                            mSProviderInfo.breed_type = jsonArray.getJSONObject(i).getString("breed_type");
//                            mSProviderInfo.vaccinated = jsonArray.getJSONObject(i).getString("vaccinated");
                            mPetIndividualInfo.contact_email = jsonArray.getJSONObject(i).getString("contact_email");
                            mPetIndividualInfo.contact_address = jsonArray.getJSONObject(i).getString("contact_address");
                            mPetIndividualInfo.contact_phone = jsonArray.getJSONObject(i).getString("contact_phone");
                            mPetIndividualInfo.contact_web = jsonArray.getJSONObject(i).getString("contact_web");
                            mPetIndividualInfo.favourite = jsonArray.getJSONObject(i).getInt("favourite");

                            JSONArray jsonImages = jsonArray.getJSONObject(i).getJSONArray("extra_img");
                            List<String> imagesList = new ArrayList<>();
                            for (int k=0; k<jsonImages.length(); k++){

                                imagesList.add(jsonImages.getString(k));
                                mPetIndividualInfo.imageList.add(jsonImages.getString(k));

                            }

                            JSONArray jsonServCat = jsonArray.getJSONObject(i).getJSONArray("services_category");
                            List<String> catlist = new ArrayList<>();
                            for (int k=0; k<jsonServCat.length(); k++){

                                catlist.add(jsonServCat.getString(k));
                                mPetIndividualInfo.serviceCatList.add(jsonServCat.getJSONObject(k).getString("service_cat_name"));

                            }
                            Log.i("favo", "" + mPetIndividualInfo.favourite);

                            updateUI(mPetIndividualInfo);
                            swipeAdapterImage = new SwipeAdapterPetIndividual(getSupportFragmentManager());

                            PetIndividualInfo info = new PetIndividualInfo();
                            info.imageList.addAll(mPetIndividualInfo.imageList);
                            swipeAdapterImage.setInfo(info);

                            viewPagerImage.setAdapter(swipeAdapterImage);
                            indicator.setViewPager(viewPagerImage);

                            if(mPetIndividualInfo.favourite == 1) {

                                ivFav.setImageResource(R.drawable.heartcolor);

                            } else if(mPetIndividualInfo.favourite == 0) {

                                ivFav.setImageResource(R.drawable.heart);

                            }
                        }

                    }
                } catch (JSONException e) {

                    closeDialog();
                    Toast.makeText(ServiceProviderInfoActivity_copy.this, "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
        }

    }

    private class setFavouriteTask extends AsyncTask<Integer,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showDialog();

        }

        @Override
        protected String doInBackground(Integer... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "favorite_pets")
                    .appendQueryParameter("post_id",""+ params[0])
                    .appendQueryParameter("user_id",""+ SharedDataSaveLoad.load(ServiceProviderInfoActivity_copy.this,getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s))

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {
                        ivFav.setImageResource(R.drawable.heartcolor);
                        closeDialog();
                        Toast.makeText(getApplicationContext(), jObj.getString("success_status"), Toast.LENGTH_SHORT).show();
                    } else {

                        mPetIndividualInfo.favourite = 1;
                        ivFav.setImageResource(R.drawable.heartcolor);
                        closeDialog();
                        Toast.makeText(getApplicationContext(), jObj.getString("success_status"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {

                    closeDialog();
                    Toast.makeText(ServiceProviderInfoActivity_copy.this, "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
        }

    }
}
