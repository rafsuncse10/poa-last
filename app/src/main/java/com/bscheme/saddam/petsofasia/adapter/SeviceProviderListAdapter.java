package com.bscheme.saddam.petsofasia.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.activity.service.ServiceProviderInfoActivity;
import com.bscheme.saddam.petsofasia.activity.service.ServiceProviderInfoActivity_copy;
import com.bscheme.saddam.petsofasia.model.ServiceProviderListInfo;

import java.util.List;

/**
 * Created by Bscheme on 2/8/16.
 */
public class SeviceProviderListAdapter extends BaseAdapter {

    Context mContext;
    public SeviceProviderListAdapter(Context context){
        this.mContext = context;
    }
    public static abstract class Row {}

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final ServiceProviderListInfo text;

        public Item(ServiceProviderListInfo text) {
            this.text = text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }


    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }
    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View view = convertView;

        if (getItemViewType(position) == 0) { // Item
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_item_service_provider, parent, false);
            }

            final Item item = (Item) getItem(position);
            TextView tvTitle = (in.workarounds.typography.TextView) view.findViewById(R.id.tv_title);
            TextView tvShelter = (in.workarounds.typography.TextView) view.findViewById(R.id.tvs_shelter);
            TextView tvLocation = (in.workarounds.typography.TextView) view.findViewById(R.id.tv_location);
            tvTitle.setText(item.text.title);

            if (item.text.serviceList != null)
                tvShelter.setText(item.text.serviceList.get(0));

            tvLocation.setText(item.text.location);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent providerIntent = new Intent(mContext, ServiceProviderInfoActivity.class);
                    providerIntent.putExtra("info_serv", item.text);
                    mContext.startActivity(providerIntent);
                }
            });
        } else { // Section
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = (LinearLayout) inflater.inflate(R.layout.row_item_section_alphabet, parent, false);
            }


            Section section = (Section) getItem(position);
            TextView textView = (TextView) view.findViewById(R.id.txt_alphabet_sort);
            textView.setText(section.text.toUpperCase());
        }

        return view;
    }
}
