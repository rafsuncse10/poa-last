package com.bscheme.saddam.petsofasia;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.activity.PetIndividualInfoActivity;
import com.bscheme.saddam.petsofasia.activity.service.ServiceProviderInfoActivity;
import com.bscheme.saddam.petsofasia.model.PetIndividualInfo;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */

public class PetIndividualFragment extends android.support.v4.app.Fragment {

    PetIndividualInfo mPetIndividualInfo;
    int position;
    public PetIndividualFragment newInstance(PetIndividualInfo petIndividualInfo, int imagePosition){
        PetIndividualFragment fragment = new PetIndividualFragment();
        Bundle args = new Bundle();
        args.putParcelable("infos",petIndividualInfo);
        args.putInt("imgPosition", imagePosition);
        fragment.setArguments(args);

        return fragment;
    }
    public PetIndividualFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pet_individual, container, false);

        Bundle bundle = getArguments();
        String message = Integer.toString(bundle.getInt("countTwo"));



        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPetIndividualInfo = new PetIndividualInfo();

        mPetIndividualInfo = getArguments().getParcelable("infos");
        position = getArguments().getInt("imgPosition");

        Log.i("position",""+position);
        ImageView petImages = (ImageView) view.findViewById(R.id.imgPetsFragment);
//        Picasso.with(getActivity()).load(mPetIndividualInfo.imageList.get(0)).into(petImages);

        try{
            if (mPetIndividualInfo.imageList.size()> 0)

                Picasso.with(getActivity()).load(mPetIndividualInfo.imageList.get(position)).into(petImages);
//                petImages.setImageURI(Uri.parse(mPetIndividualInfo.imageList.get(position)));

        }catch (NullPointerException e){
            petImages.setImageResource(R.drawable.petasialogotwo);
        }
        petImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("IMView", getActivity().getLocalClassName());
                if( getActivity().getClass().equals(PetIndividualInfoActivity.class)
                    || getActivity().getClass().equals(ServiceProviderInfoActivity.class)) {
                    Intent i = new Intent(getActivity(), PetIndividualView.class);
                    i.putExtra("position", position);
                    i.putExtra("info_pets", mPetIndividualInfo);
                    startActivity(i);
                }
            }
        });
    }
}
