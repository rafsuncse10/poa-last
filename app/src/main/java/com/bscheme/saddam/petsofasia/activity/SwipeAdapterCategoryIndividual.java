package com.bscheme.saddam.petsofasia.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bscheme.saddam.petsofasia.fragment.CategoryIndividualFragment;

public class SwipeAdapterCategoryIndividual extends FragmentStatePagerAdapter {

    public SwipeAdapterCategoryIndividual(FragmentManager fm) {
        super(fm);
    }

    public Fragment getItem(int i) {
        Fragment fragmentThree = new CategoryIndividualFragment();
        Bundle bundleThree = new Bundle();
        bundleThree.putInt("countThree", i + 1);
        fragmentThree.setArguments(bundleThree);
        return fragmentThree;
    }

    @Override
    public int getCount() {
        return 4;
    }

}
