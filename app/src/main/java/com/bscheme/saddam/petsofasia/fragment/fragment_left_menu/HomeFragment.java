package com.bscheme.saddam.petsofasia.fragment.fragment_left_menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.fragment.PetsListFragment;
import com.bscheme.saddam.petsofasia.fragment.ServicesListFragment;

import in.workarounds.typography.Button;

/**
 * A simple {@link Fragment} subclass.
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    Button btnPets,btnServices;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myInflatedView = inflater.inflate(R.layout.fragment_home, container, false);

        return myInflatedView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnPets = (Button) view.findViewById(R.id.btn_pets_tab);
        btnServices = (Button) view.findViewById(R.id.btn_service_tab);

        btnPets.setTextColor(getActivity().getResources().getColor(R.color.tab_active));
        btnPets.setCompoundDrawablesWithIntrinsicBounds(null, getActivity().getResources().getDrawable(R.drawable.ic_tab_pets_color), null, null);

        fragmentContainerAdd(new PetsListFragment());

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnServices.setOnClickListener(this);
        btnPets.setOnClickListener(this);
    }

    public void fragmentContainerAdd(Fragment fragment){

        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.tab_container,fragment).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_pets_tab:
                btnPets.setTextColor(getActivity().getResources().getColor(R.color.tab_active));
                btnPets.setCompoundDrawablesWithIntrinsicBounds(null, getActivity().getResources().getDrawable(R.drawable.ic_tab_pets_color), null, null);
                btnServices.setTextColor(getActivity().getResources().getColor(R.color.tab_inactive));
                btnServices.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_services, 0, 0);

                fragmentContainerAdd(new PetsListFragment());
                return;
            case R.id.btn_service_tab:
                btnServices.setTextColor(getActivity().getResources().getColor(R.color.tab_active));
                btnServices.setCompoundDrawablesWithIntrinsicBounds(null, getActivity().getResources().getDrawable(R.drawable.ic_tab_services_color), null, null);
                btnPets.setTextColor(getActivity().getResources().getColor(R.color.tab_inactive));
                btnPets.setCompoundDrawablesWithIntrinsicBounds(null, getActivity().getResources().getDrawable(R.drawable.ic_tab_pets), null, null);

                fragmentContainerAdd(new ServicesListFragment());
                return;
        }
    }
}
