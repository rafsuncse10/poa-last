package com.bscheme.saddam.petsofasia.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bscheme.petsofasia.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryIndividualFragment extends android.support.v4.app.Fragment {


    public CategoryIndividualFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_individual, container, false);
        ImageView imgCategoryFragment = (ImageView) view.findViewById(R.id.imgCategoryFragment);

        Bundle bundle = getArguments();
        String message = Integer.toString(bundle.getInt("countThree"));

        if(message.equals("1")) {
            imgCategoryFragment.setImageResource(R.drawable.raocaninedogtrainingpetstoretwo);
        }

        else if (message.equals("2")){
            imgCategoryFragment.setImageResource(R.drawable.raocaninedogtrainingpetstoretwo);
        }

        else if (message.equals("3")) {
            imgCategoryFragment.setImageResource(0);
        }

        else if (message.equals("4")) {
            imgCategoryFragment.setImageResource(0);
        }

        return view;
    }


}
