package com.bscheme.saddam.petsofasia.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bscheme.petsofasia.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceCategoryListingFragment extends Fragment {


    public ServiceCategoryListingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myInflatedView = inflater.inflate(R.layout.fragment_service_category_listing, container, false);

        return myInflatedView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*final LinearLayout button = (LinearLayout) view.findViewById(R.id.homeFragment);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Fragment newFragment = new HomeFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.container, newFragment);
                transaction.addToBackStack(null);

                transaction.commit();

            }

        });
*/
        /*final LinearLayout buttonTwo = (LinearLayout) view.findViewById(R.id.servicesCategoryListingFragment);
        buttonTwo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), ServiceProviderListActivity.class);
                startActivity(i);

            }

        });
*/
    }
}
