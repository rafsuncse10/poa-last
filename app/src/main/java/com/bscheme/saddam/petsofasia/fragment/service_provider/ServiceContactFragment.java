package com.bscheme.saddam.petsofasia.fragment.service_provider;


import android.app.Fragment;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.model.ServiceProviderInfo;

import java.net.MalformedURLException;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceContactFragment extends android.support.v4.app.Fragment {


    ServiceProviderInfo mProviderInfo;
    ImageView ivFacebook,ivTwitter,ivInstagram,ivDial,ivWeb,ivEmail;

    public ServiceContactFragment() {
        // Required empty public constructor
    }

    public static ServiceContactFragment newInstance(ServiceProviderInfo providerInfo) {

        ServiceContactFragment f = new ServiceContactFragment();
        Bundle b = new Bundle();
        b.putParcelable("info_provider", providerInfo);

        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProviderInfo = (ServiceProviderInfo) getArguments().getParcelable("info_provider");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View myInflatedView = inflater.inflate(R.layout.fragment_service_contact, container, false);

        TextView txtPhone = (TextView) myInflatedView.findViewById(R.id.txtPhone);
        txtPhone.setText(Html.fromHtml(mProviderInfo.contact_phone));

        TextView txtWebsite = (TextView) myInflatedView.findViewById(R.id.txtWebsite);
        txtWebsite.setText(Html.fromHtml(mProviderInfo.contact_web));

        TextView txtInfo = (TextView) myInflatedView.findViewById(R.id.txtInfo);
        txtInfo.setText(Html.fromHtml(mProviderInfo.contact_email));

        return myInflatedView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ivFacebook = (ImageView) view.findViewById(R.id.imgFacebook);
        ivTwitter = (ImageView) view.findViewById(R.id.imgTwitter);
        ivInstagram = (ImageView) view.findViewById(R.id.imgInstagram);
        ivDial = (ImageView) view.findViewById(R.id.imgDial);
        ivWeb = (ImageView) view.findViewById(R.id.imgWebsite);
        ivEmail = (ImageView) view.findViewById(R.id.imgEmail);

        ivFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInBrowser(mProviderInfo.facebookLink);
            }
        });
        ivTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInBrowser(mProviderInfo.twitterLink);
            }
        });
        ivInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInBrowser(mProviderInfo.instagramLink);
            }
        });

        ivDial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialar(mProviderInfo.contact_phone);
            }
        });
        ivWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInBrowser(mProviderInfo.contact_web);
            }
        });
        ivEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEmailApp(mProviderInfo.contact_email);
            }
        });

    }

    private void openInBrowser(String link){

        if (!link.contains("http://"))
        link = "http://" + link;

        URL url = null;
        try {
            url = new URL(link);
        } catch (MalformedURLException e) {
            Log.v("myApp", "bad url entered");
        }
        if (url == null)
            Toast.makeText(getActivity(),"Please add valid link",Toast.LENGTH_SHORT).show();
        else {
            Uri uri = Uri.parse(url.toString()); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            getActivity().startActivity(intent);
        }

    }

    private void openDialar(String dialNumber){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+dialNumber));
        startActivity(intent);
    }

    private void openEmailApp(String email) {
        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

/* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{email});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Adopt pets");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Add message here");

/* Send it off to the Activity-Chooser */
        getActivity().startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }
}
