package com.bscheme.saddam.petsofasia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.activity.service.ServiceProviderInfoActivity;
import com.bscheme.saddam.petsofasia.model.ServiceProviderListInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.workarounds.typography.TextView;

/**
 * Created by ASUS on 10/18/2015.
 */
public class FavouriteServiceProviderAdapter extends RecyclerView.Adapter<FavouriteServiceProviderAdapter.PolicyViewHolder> {


    public Context mContext;

    List<ServiceProviderListInfo> ids = new ArrayList();


    public FavouriteServiceProviderAdapter(Context paramContext, List<ServiceProviderListInfo> ids) {
        this.mContext = paramContext;
        this.ids = ids;
    }


    @Override
    public PolicyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        return new PolicyViewHolder(mInflater.inflate(R.layout.row_item_fav_pet, parent, false));
    }

    @Override
    public void onBindViewHolder(PolicyViewHolder holder, int paramInt) {

        Picasso.with(mContext).load(ids.get(paramInt).imgLink).into(holder.imgLike);
        holder.tvTitle.setText(ids.get(paramInt).title);
        holder.tvShelter.setText("Shelter: "+ids.get(paramInt).serviceList.get(0));
        holder.tvLocation.setText("Location: "+ids.get(paramInt).location);

    }

    @Override
    public int getItemCount() {
        return this.ids.size();
    }

    public class PolicyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imgLike;
        TextView tvTitle,tvShelter,tvLocation;

        public PolicyViewHolder(View paramView) {
            super(paramView);
            imgLike = (ImageView) paramView.findViewById(R.id.iv_thumb);
            tvTitle = (TextView) paramView.findViewById(R.id.tv_title);
            tvShelter = (TextView) paramView.findViewById(R.id.tvs_shelter);
            tvLocation = (TextView) paramView.findViewById(R.id.tv_location);

            paramView.setOnClickListener(this);
        }

        public void onClick(View view) {
            int pos = getAdapterPosition();
            Log.i("clidk child", "click " + pos);
            Intent in = new Intent(mContext, ServiceProviderInfoActivity.class);
            in.putExtra("info_serv", ids.get(pos));
            mContext.startActivity(in);

        }
    }

}
