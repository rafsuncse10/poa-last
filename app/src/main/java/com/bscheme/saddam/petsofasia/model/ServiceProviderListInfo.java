package com.bscheme.saddam.petsofasia.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Bscheme on 2/4/16.
 */
public class ServiceProviderListInfo implements Parcelable {

    public String title;
    public int id;
    public String imgLink;
    public String location;
    public ArrayList<String> serviceList = new ArrayList<>();
    public ServiceProviderListInfo(){

    }
    protected ServiceProviderListInfo(Parcel in) {
        id = in.readInt();
        title = in.readString();
        imgLink = in.readString();
        location = in.readString();
        in.readStringList(serviceList);
    }

    public static final Creator<ServiceProviderListInfo> CREATOR = new Creator<ServiceProviderListInfo>() {
        @Override
        public ServiceProviderListInfo createFromParcel(Parcel in) {
            return new ServiceProviderListInfo(in);
        }

        @Override
        public ServiceProviderListInfo[] newArray(int size) {
            return new ServiceProviderListInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(imgLink);
        dest.writeString(location);
        dest.writeStringList(serviceList);
    }
}
