package com.bscheme.saddam.petsofasia;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.model.PetIndividualInfo;

import me.relex.circleindicator.CircleIndicator;

public class PetIndividualView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_individual_view);

        int s = getIntent().getIntExtra("position",0);
        PetIndividualInfo mPetIndividualInfo = getIntent().getParcelableExtra("info_pets");

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        SwipeAdapterPetIndividual swipeAdapter = new SwipeAdapterPetIndividual(getSupportFragmentManager());
        swipeAdapter.setInfo(mPetIndividualInfo);
        viewPager.setAdapter(swipeAdapter);


        viewPager.setCurrentItem(s);

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator_default);
        indicator.setViewPager(viewPager);
    }

    public void onClickEnlarge(View view) {

    }

    public void onClose(View view) {
        finish();
    }

}
