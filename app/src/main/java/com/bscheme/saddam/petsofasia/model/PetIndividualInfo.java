package com.bscheme.saddam.petsofasia.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bscheme on 2/4/16.
 */
public class PetIndividualInfo implements Parcelable {

    public String title;
    public int id;
    public String about;
    public String featured_image;
    public String location;
    public String shelter;
    public String type;
    public String gender;
    public String age_range;
    public String size_type;
    public String breed_type;
    public String vaccinated;
    public String contact_email;
    public String contact_address;
    public String contact_phone;
    public String contact_web;
    public int favourite;

    public List<String> imageList = new ArrayList<>();

    public PetIndividualInfo(){

    }
    protected PetIndividualInfo(Parcel in) {
        id = in.readInt();
        title = in.readString();
        about = in.readString();
        featured_image = in.readString();
        location = in.readString();
        shelter = in.readString();
        type = in.readString();
        gender = in.readString();
        age_range = in.readString();
        size_type = in.readString();
        breed_type = in.readString();
        vaccinated = in.readString();
        contact_email = in.readString();
        contact_address = in.readString();
        contact_phone = in.readString();
        contact_web = in.readString();
        favourite = in.readInt();

        imageList = new ArrayList<>();
        in.readStringList(imageList);
    }

    public static final Creator<PetIndividualInfo> CREATOR = new Creator<PetIndividualInfo>() {
        @Override
        public PetIndividualInfo createFromParcel(Parcel in) {
            return new PetIndividualInfo(in);
        }

        @Override
        public PetIndividualInfo[] newArray(int size) {
            return new PetIndividualInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(about);
        dest.writeString(featured_image);
        dest.writeString(location);
        dest.writeString(shelter);
        dest.writeString(type);
        dest.writeString(gender);
        dest.writeString(age_range);
        dest.writeString(size_type);
        dest.writeString(breed_type);
        dest.writeString(vaccinated);
        dest.writeString(contact_email);
        dest.writeString(contact_address);
        dest.writeString(contact_phone);
        dest.writeString(contact_web);
        dest.writeInt(favourite);

        dest.writeStringList(imageList);
    }
}
