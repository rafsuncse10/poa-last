package com.bscheme.saddam.petsofasia;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.bscheme.saddam.petsofasia.model.PetIndividualInfo;

public class SwipeAdapterPetIndividual extends FragmentPagerAdapter {

    PetIndividualInfo mPetIndividualInfo;
    public SwipeAdapterPetIndividual(FragmentManager fm,PetIndividualInfo petIndividualInfo) {
        super(fm);
        this.mPetIndividualInfo = petIndividualInfo;
    }
    public SwipeAdapterPetIndividual(FragmentManager fm) {
        super(fm);
        mPetIndividualInfo = new PetIndividualInfo();
    }

    public Fragment getItem(int i) {
        PetIndividualFragment fragmentTwo = new PetIndividualFragment().newInstance(mPetIndividualInfo,i);
        return fragmentTwo;
    }

    @Override
    public int getCount() {
        Log.i("list items",""+mPetIndividualInfo.imageList.size());
        return mPetIndividualInfo.imageList.size();
    }
    public void setInfo(PetIndividualInfo petIndividualInfo){
        this.mPetIndividualInfo = petIndividualInfo;
    }

}
