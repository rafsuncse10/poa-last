package com.bscheme.saddam.petsofasia.activity;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.adapter.SwipeAdapter;

import me.relex.circleindicator.CircleIndicator;

public class SplashPage extends FragmentActivity {

    LinearLayout btnLogin,btnSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_page);

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        SwipeAdapter swipeAdapter = new SwipeAdapter(getSupportFragmentManager());
        viewPager.setAdapter(swipeAdapter);

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator_default);
        indicator.setViewPager(viewPager);

        btnLogin = (LinearLayout) findViewById(R.id.logIn);
        btnSignup = (LinearLayout) findViewById(R.id.createAnAccount);

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SplashPage.this, CreateAccount.class);
                startActivityForResult(i,0);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SplashPage.this, LogIn.class);
                startActivityForResult(i,0);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 1){
            finish();
        }
    }
}
