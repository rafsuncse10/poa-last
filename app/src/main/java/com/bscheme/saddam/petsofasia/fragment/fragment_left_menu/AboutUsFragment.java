package com.bscheme.saddam.petsofasia.fragment.fragment_left_menu;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.utils.CallDialBrowserIntents;

import in.workarounds.typography.TextView;

/**
 * A simple {@link Fragment} subclass.
 */

public class AboutUsFragment extends android.support.v4.app.Fragment {

    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myInflatedView = inflater.inflate(R.layout.fragment_about_us, container, false);

        TextView contentAboutUs = (TextView) myInflatedView.findViewById(R.id.contentAboutUs);
        contentAboutUs.setText("\n\n\n\n\n" +
                               "The space here in this AboutUsFragment is set for displaying the About Us content for this application." +
                               "\n\n\n\n\n");

        final TextView txtPhone = (TextView) myInflatedView.findViewById(R.id.txtPhone);
        String htmlPhone = "<u>+65 0000 0000</u>";
        txtPhone.setText(Html.fromHtml(htmlPhone));
        txtPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CallDialBrowserIntents(getActivity()).openDialar(txtPhone.getText().toString());
            }
        });
        final TextView txtWebsite = (TextView) myInflatedView.findViewById(R.id.txtWebsite);
        String htmlWebsite = "<u>www.petsofasia.com</u>";
        txtWebsite.setText(Html.fromHtml(htmlWebsite));
        txtWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CallDialBrowserIntents(getActivity()).openInBrowser(txtWebsite.getText().toString());
            }
        });


        final TextView txtInfo = (TextView) myInflatedView.findViewById(R.id.txtInfo);
        String htmlInfo = "<u>info@petsofasia.com</u>";
        txtInfo.setText(Html.fromHtml(htmlInfo));
        txtInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CallDialBrowserIntents(getActivity()).openEmailApp(txtInfo.getText().toString());
            }
        });

        return myInflatedView;
    }

}
