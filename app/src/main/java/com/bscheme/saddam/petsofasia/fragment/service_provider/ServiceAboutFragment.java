package com.bscheme.saddam.petsofasia.fragment.service_provider;


import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.model.ServiceProviderInfo;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceAboutFragment extends android.support.v4.app.Fragment {


    ServiceProviderInfo mProviderInfo;
    LinearLayout container;

    public ServiceAboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View myInflatedView = inflater.inflate(R.layout.fragment_service_about, container, false);

        if (getArguments() != null) {
            mProviderInfo = (ServiceProviderInfo) getArguments().getParcelable("info_provider");
        }

//        myInflatedView.setMinimumHeight(700);

        TextView txtServiceIndividualDescriptionContent = (TextView) myInflatedView.findViewById(R.id.txtServiceIndividualDescriptionContent);
        txtServiceIndividualDescriptionContent.setText(mProviderInfo.about);
        TextView txtServiceIndividualCategoryContent = (TextView) myInflatedView.findViewById(R.id.txtServiceIndividualCategoryContent);

        StringBuilder stringBuilder = new StringBuilder();

        container = (LinearLayout) myInflatedView.findViewById(R.id.container);

        for (int i=0; i<mProviderInfo.serviceCatList.size(); i++){

            LinearLayout horizontal = new LinearLayout(getActivity());
            TextView text = new TextView(getActivity());
            ImageView img = new ImageView(getActivity());

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.CENTER_VERTICAL;
            text.setLayoutParams(params);
            text.setText(mProviderInfo.serviceCatList.get(i));


            img.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            img.setImageResource(R.drawable.servicespoint);
            img.setPadding(5, 5, 5, 5);

            horizontal.setLayoutParams(params);
            horizontal.addView(img);
            horizontal.addView(text);
            container.addView(horizontal);
            Log.i("data", mProviderInfo.serviceCatList.get(i));

            stringBuilder.append(mProviderInfo.serviceCatList.get(i));
            if ( mProviderInfo.serviceCatList.size()>1 && i < mProviderInfo.serviceCatList.size()-1){
                stringBuilder.append(", ");
            }
        }
        txtServiceIndividualCategoryContent.setText(Html.fromHtml(stringBuilder.toString()));

        return myInflatedView;
    }

    public static ServiceAboutFragment newInstance(ServiceProviderInfo providerInfo) {

        ServiceAboutFragment f = new ServiceAboutFragment();
        Bundle b = new Bundle();
        b.putParcelable("info_provider", providerInfo);

        f.setArguments(b);

        return f;
    }

}
