package com.bscheme.saddam.petsofasia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.activity.service.ServiceProviderListActivity;
import com.bscheme.saddam.petsofasia.model.ServiceListInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 10/18/2015.
 */
public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.PolicyViewHolder> {


    public Context mContext;

    List<ServiceListInfo> ids = new ArrayList();


    public ServiceListAdapter(Context paramContext, List<ServiceListInfo> ids) {
        this.mContext = paramContext;
        this.ids = ids;
    }


    @Override
    public PolicyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        return new PolicyViewHolder(mInflater.inflate(R.layout.row_item_service_list, parent, false));
    }

    @Override
    public void onBindViewHolder(PolicyViewHolder holder, int paramInt) {

        holder.title.setText(Html.fromHtml(ids.get(paramInt).title));
        Picasso.with(mContext).load(ids.get(paramInt).imgLink).into(holder.imgLike);

    }

    @Override
    public int getItemCount() {
        return this.ids.size();
    }

    public class PolicyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imgLike;
        public in.workarounds.typography.TextView title;

        public PolicyViewHolder(View paramView) {
            super(paramView);
            imgLike = (ImageView) paramView.findViewById(R.id.img_pet_image);
            title = (in.workarounds.typography.TextView) paramView.findViewById(R.id.txt_title);
            paramView.setOnClickListener(this);
        }

        public void onClick(View paramView) {
            int pos = getAdapterPosition();

            Intent in = new Intent(mContext, ServiceProviderListActivity.class);
            in.putExtra("info", ids.get(pos));
            mContext.startActivity(in);
        }
    }

}
