package com.bscheme.saddam.petsofasia.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bscheme.petsofasia.R;

import in.workarounds.typography.TextView;

/**
 * A simple {@link Fragment} subclass.
 */

public class SplashPageFragment extends Fragment {

    public SplashPageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_splash_page, container, false);
        TextView splashPageFragment = (TextView) view.findViewById(R.id.splashPageFragment);

        Bundle bundle = getArguments();
        String message = Integer.toString(bundle.getInt("count"));

        if(message.equals("1")) {
            splashPageFragment.setText("Cum sociis natoque penatibus et" +
                    " magnis dis parturient montes," +
                    " nascetur ridiculus mus. Sed" +
                    " posuere consectetur est at lobortis.");
        }

        else if (message.equals("2")) {
            splashPageFragment.setText("The space here in this SplashPageFragment" +
                                       " is set for displaying the Page TWO content" +
                                       " for this application.");
        }

        else if (message.equals("3")) {
            splashPageFragment.setText("The space here in this SplashPageFragment" +
                                       " is set for displaying the Page THREE content" +
                                       " for this application.");
        }

        return view;
    }

}
