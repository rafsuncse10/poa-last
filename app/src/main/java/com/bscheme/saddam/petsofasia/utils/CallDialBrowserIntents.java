package com.bscheme.saddam.petsofasia.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by kanchan on 2/16/2016.
 */
public class CallDialBrowserIntents {
    private Activity caller;
    public CallDialBrowserIntents(Activity caller){
        this.caller = caller;
    }
    public void openInBrowser(String link) {

        if (!link.contains("http://"))
            link = "http://" + link;

        URL url = null;
        try {
            url = new URL(link);
        } catch (MalformedURLException e) {
            Log.v("myApp", "bad url entered");
        }
        if (url == null)
            Toast.makeText(caller, "Please add valid link", Toast.LENGTH_SHORT).show();
        else {
            Uri uri = Uri.parse(url.toString()); // missing 'http://' will cause crashed
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            caller.startActivity(intent);
        }

    }

    public void openDialar(String dialNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+dialNumber));
        caller.startActivity(intent);

        try {
            caller.startActivity(intent);
        } catch (Exception e) {
            /*intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(dialNumber));
            *//*if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }*//*
            startActivity(intent);
            */
        }
    }

    public void openEmailApp(String email) {
        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

/* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{email});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Adopt pets");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Add message here");

/* Send it off to the Activity-Chooser */
        caller.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }
}
