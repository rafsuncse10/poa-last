package com.bscheme.saddam.petsofasia.activity;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.model.PetIndividualInfo;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenConfirmationDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import in.workarounds.typography.Button;
import in.workarounds.typography.EditText;
import in.workarounds.typography.TextView;


public class MessageToPets extends AppCompatActivity {

    TextView txtHeaders,petName;
    Button btnSend;
    ImageView pet_img;
    EditText edtMsg;
    PetIndividualInfo mPetIndividualInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        mPetIndividualInfo = getIntent().getParcelableExtra("pets_info");

        txtHeaders = (TextView) findViewById(R.id.txtHeaders);
        petName = (TextView) findViewById(R.id.txt_pet_name);
        btnSend = (Button) findViewById(R.id.btn_send);
        pet_img = (ImageView) findViewById(R.id.iv_pet_img);
        edtMsg = (EditText) findViewById(R.id.edt_message);

        petName.setText(mPetIndividualInfo.title);

        Picasso.with(this).load(""+mPetIndividualInfo.featured_image).into(pet_img);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(MessageToPets.this, Confirmation.class);
//                startActivity(i);
                if ( !TextUtils.isEmpty(edtMsg.getText().toString())){
                    new sendEmailTask().execute(mPetIndividualInfo.contact_email,"Adopt "+mPetIndividualInfo.title.toUpperCase(Locale.ENGLISH),edtMsg.getText().toString());
                }else {
                    Toast.makeText(MessageToPets.this,"Please write a msg !!!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onClickBack(View view) {
        finish();
    }

    void showDialog() {

        FullScreenConfirmationDialog fragment = (FullScreenConfirmationDialog) getSupportFragmentManager().findFragmentByTag("dialog_confirmation");
        if (fragment == null) {
            fragment = new FullScreenConfirmationDialog();
            fragment.setCancelable(false);
            getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog_confirmation")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }
    void closeDialog(){
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog_confirmation");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
        }
//        ((FullScreenConfirmationDialog)prev).dismiss();
    }

    private class sendEmailTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "send_email")
                    .appendQueryParameter("to_mail", "" + params[0])
                    .appendQueryParameter("from_mail", "" + SharedDataSaveLoad.load(MessageToPets.this, getResources().getString(R.string.shared_pref_user_email)))
                    .appendQueryParameter("subject", ""+params[1])
                    .appendQueryParameter("message", ""+params[2]);
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s))

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {
                        Toast.makeText(MessageToPets.this, "Message not send, Try again !!!", Toast.LENGTH_SHORT).show();
                    } else {

                        showDialog();

                    }
                } catch (JSONException e) {

                    Toast.makeText(MessageToPets.this, "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
        }

    }

}
