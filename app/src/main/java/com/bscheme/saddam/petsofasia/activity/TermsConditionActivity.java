package com.bscheme.saddam.petsofasia.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.fragment.fragment_left_menu.TermsConditionsFragment;

public class TermsConditionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);

        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,new TermsConditionsFragment()).commit();
    }
    public void onClickBack(View view) {
        finish();
    }

}
