package com.bscheme.saddam.petsofasia.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import com.bscheme.petsofasia.R;

import me.relex.circleindicator.CircleIndicator;


public class ServiceIndividualView extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_individual_view);

//        getSupportActionBar().hide();

        ViewPager viewPagerTwo = (ViewPager) findViewById(R.id.view_pager);
        SwipeAdapterCategoryIndividual swipeAdapterTwo = new SwipeAdapterCategoryIndividual(getSupportFragmentManager());
        viewPagerTwo.setAdapter(swipeAdapterTwo);

        int t = getIntent().getIntExtra("positionTwo", viewPagerTwo.getCurrentItem());
        viewPagerTwo.setCurrentItem(t);

        CircleIndicator indicatorTwo = (CircleIndicator) findViewById(R.id.indicator_default);
        indicatorTwo.setViewPager(viewPagerTwo);
    }

    public void onClickEnlarge(View view) {

    }

    public void onClose(View view) {
        finish();
    }
}
