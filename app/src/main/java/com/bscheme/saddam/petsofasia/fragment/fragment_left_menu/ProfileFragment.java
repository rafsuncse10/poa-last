package com.bscheme.saddam.petsofasia.fragment.fragment_left_menu;

import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;

import org.json.JSONException;
import org.json.JSONObject;

import in.workarounds.typography.Button;
import in.workarounds.typography.EditText;
import in.workarounds.typography.TextView;

/**
 * A simple {@link Fragment} subclass.
 */

public class ProfileFragment extends android.support.v4.app.Fragment implements View.OnClickListener {

    EditText editTextFullName,editTextEmail,editTextPassword;
    Button button;

    TextView tv_pass_title;
    int check = 0;
    public ProfileFragment() {
        // Required empty public constructor
    }@Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myInflatedView = inflater.inflate(R.layout.fragment_profile, container, false);

        tv_pass_title = (TextView) myInflatedView.findViewById(R.id.tv_pass_title);

        editTextFullName = (EditText) myInflatedView.findViewById(R.id.editTextFullName);
        editTextFullName.setFocusable(false);
        editTextFullName.setGravity(Gravity.RIGHT);
        editTextFullName.setBackgroundColor(Color.TRANSPARENT);

        editTextEmail = (EditText) myInflatedView.findViewById(R.id.editTextEmail);
        editTextEmail.setFocusable(false);
        editTextEmail.setGravity(Gravity.RIGHT);
        editTextEmail.setBackgroundColor(Color.TRANSPARENT);

        editTextPassword = (EditText) myInflatedView.findViewById(R.id.editTextPassword);
        editTextPassword.setFocusable(false);
        editTextPassword.setGravity(Gravity.RIGHT);
        editTextPassword.setBackgroundColor(Color.TRANSPARENT);

        button = (Button) myInflatedView.findViewById(R.id.btnEditProfile);

        editTextEmail.setText(""+SharedDataSaveLoad.load(getActivity(),getActivity().getResources().getString(R.string.shared_pref_user_email)));
        editTextFullName.setText(""+SharedDataSaveLoad.load(getActivity(),getActivity().getResources().getString(R.string.shared_pref_user_name)));

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(check == 0) {

                    int imgResource = R.drawable.savechanges;
                    button.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                    button.setBackgroundResource((R.drawable.button_green));
                    button.setText("SAVE CHANGES");
                    button.setPadding(20, 0, 20, 0);

                    editTextFullName.setFocusableInTouchMode(true);
                    editTextFullName.setFocusable(true);
                    editTextFullName.setEnabled(true);
                    editTextFullName.setCursorVisible(true);
                    editTextFullName.setGravity(Gravity.RIGHT);

                    tv_pass_title.setText("CURRENT PASSWORD");
                    editTextPassword.setFocusableInTouchMode(true);
                    editTextPassword.setFocusable(true);
                    editTextPassword.setEnabled(true);
                    editTextPassword.setCursorVisible(true);
                    editTextPassword.setGravity(Gravity.RIGHT);


                    check = 1;

                } else if(check == 1) {

                    int imgResource = R.drawable.editprofile;

                    button.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
                    button.setBackgroundResource((R.drawable.button_orange));
                    button.setText("EDIT PROFILE");
                    button.setPadding(30, 0, 30, 0);

                    editTextFullName.setFocusable(false);
                    editTextFullName.setEnabled(false);
                    editTextFullName.setCursorVisible(false);

                    tv_pass_title.setText("PASSWORD");
                    editTextPassword.setFocusable(false);
                    editTextPassword.setEnabled(false);
                    editTextPassword.setCursorVisible(false);

                    /*InputMethodManager inputManager = (InputMethodManager)
                            getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);*/

                    new sendUpdateProfileInfoTask().execute(editTextFullName.getText().toString(),editTextPassword.getText().toString());
                    check = 0;

                }

            }

        });

        return myInflatedView;
    }

    @Override
    public void onClick(View v) {

    }

    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(false);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }
    void closeDialog(){
        android.support.v4.app.Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getActivity().getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
//            prev.dismiss();
            Log.i("frag diag", "close");
        }
    }

    private class sendUpdateProfileInfoTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showDialog();

        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "update_user_name")
                    .appendQueryParameter("full_name", "" + params[0])
                    .appendQueryParameter("current_password", "" + params[1])
                    .appendQueryParameter("user_id", ""+ SharedDataSaveLoad.load(getActivity(), getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            closeDialog();

            if (!TextUtils.isEmpty(s)) {

                Log.i("response servPro", s);
                try {
                    JSONObject jObj = new JSONObject(s);
                    if (jObj.getInt("success") == 1) {
                        Toast.makeText(getActivity(), "" + jObj.getString("status"), Toast.LENGTH_SHORT).show();
                        SharedDataSaveLoad.save(getActivity(), getActivity().getResources().getString(R.string.shared_pref_user_email), jObj.getJSONObject("user_info").getString("user_name"));
                        SharedDataSaveLoad.save(getActivity(), getActivity().getResources().getString(R.string.shared_pref_user_name), jObj.getJSONObject("user_info").getString("name"));

                    }else if (jObj.getInt("success") == 0){
                        Toast.makeText(getActivity(), "" + jObj.getString("status"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }

}
