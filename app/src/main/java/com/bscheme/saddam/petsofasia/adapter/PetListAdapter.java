package com.bscheme.saddam.petsofasia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bscheme.saddam.petsofasia.activity.PetIndividualInfoActivity;
import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.model.PetListInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ASUS on 10/18/2015.
 */
public class PetListAdapter extends RecyclerView.Adapter<PetListAdapter.PolicyViewHolder> {


    public Context mContext;

    List<PetListInfo> ids = new ArrayList();


    public PetListAdapter(Context paramContext, List<PetListInfo> ids) {
        this.mContext = paramContext;
        this.ids = ids;
    }


    @Override
    public PolicyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        return new PolicyViewHolder(mInflater.inflate(R.layout.row_item_petlist, parent, false));
    }

    @Override
    public void onBindViewHolder(PolicyViewHolder holder, int paramInt) {

        Picasso.with(mContext).load(ids.get(paramInt).imageList.get(0)).into(holder.imgLike);

    }

    @Override
    public int getItemCount() {
        return this.ids.size();
    }

    public class PolicyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imgLike;

        public PolicyViewHolder(View paramView) {
            super(paramView);
            imgLike = (ImageView) paramView.findViewById(R.id.img_pet_image);

            paramView.setOnClickListener(this);
        }

        public void onClick(View view) {
            int pos = getAdapterPosition();
            Log.i("clidk child", "click " + pos);
            Intent in = new Intent(mContext, PetIndividualInfoActivity.class);
            in.putExtra("pets_info", ids.get(pos));
            mContext.startActivity(in);

        }
    }

}
