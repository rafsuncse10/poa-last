package com.bscheme.saddam.petsofasia.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Bscheme on 2/4/16.
 */
public class PetListInfo implements Parcelable {

    public String title;
    public int id;
    public ArrayList<String> imageList = new ArrayList<>();
    public PetListInfo(){

    }
    protected PetListInfo(Parcel in) {
        id = in.readInt();
        title = in.readString();
        in.readStringList(imageList);
    }

    public static final Creator<PetListInfo> CREATOR = new Creator<PetListInfo>() {
        @Override
        public PetListInfo createFromParcel(Parcel in) {
            return new PetListInfo(in);
        }

        @Override
        public PetListInfo[] newArray(int size) {
            return new PetListInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(id);
        dest.writeString(title);
        dest.writeStringList(imageList);
    }
}
