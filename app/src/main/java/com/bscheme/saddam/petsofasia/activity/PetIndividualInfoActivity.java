package com.bscheme.saddam.petsofasia.activity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.ExpandableView;
import com.bscheme.saddam.petsofasia.PetIndividualView;
import com.bscheme.saddam.petsofasia.SwipeAdapterPetIndividual;
import com.bscheme.saddam.petsofasia.model.PetIndividualInfo;
import com.bscheme.saddam.petsofasia.model.PetListInfo;
import com.bscheme.saddam.petsofasia.utils.CallDialBrowserIntents;
import com.bscheme.saddam.petsofasia.utils.CheckConnectivity;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.workarounds.typography.TextView;
import me.relex.circleindicator.CircleIndicator;

public class PetIndividualInfoActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    PetListInfo petListInfo;
    SweetAlertDialog mDialog;
    ViewPager viewPager;
    SwipeAdapterPetIndividual swipeAdapterTwo;
    PetIndividualInfo mPetIndividualInfo;
    ImageView ivFav;
    ImageView ivDial, ivWeb, ivEmail;
    CircleIndicator indicator;

    View viewOne, viewTwo, viewThree;
    TextView txtPetName, txtPetBreed, txtPetLocation, txtContentPets, txtLocationDetails, txtShelterDetails,
            txtPetTypeDetails, txtGenderDetails, txtAgeRangeDetails, txtSizeTypeDetails, txtBreedTypeDetails,
            txtVaccinatedDetails, txtContactShelterDetails, txtContactAddressDetails, txtContactPhoneDetails,
            txtContactWebsiteDetails, txtContactInfoDetails, txtHeaders;

    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    Geocoder geocoder = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pet_individual);

        petListInfo = getIntent().getParcelableExtra("pets_info");

        CheckConnectivity connectivity=new CheckConnectivity(PetIndividualInfoActivity.this);
        if(connectivity.isConnected())
            new gePetIndividualInfoTask().execute();
        else return;

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        indicator = (CircleIndicator) findViewById(R.id.indicator_default);

        txtHeaders = (TextView) findViewById(R.id.txtHeaders);

        txtPetName = (TextView) findViewById(R.id.txtPetName);
        txtPetBreed = (TextView) findViewById(R.id.txtPetBreed);
        txtPetLocation = (TextView) findViewById(R.id.txtPetLocation);
        txtContentPets = (TextView) findViewById(R.id.txtContentPets);
        txtLocationDetails = (TextView) findViewById(R.id.txtLocationDetails);
        txtShelterDetails = (TextView) findViewById(R.id.txtShelterDetails);
        txtPetTypeDetails = (TextView) findViewById(R.id.txtPetTypeDetails);
        txtGenderDetails = (TextView) findViewById(R.id.txtGenderDetails);
        txtAgeRangeDetails = (TextView) findViewById(R.id.txtAgeRangeDetails);
        txtSizeTypeDetails = (TextView) findViewById(R.id.txtSizeTypeDetails);
        txtBreedTypeDetails = (TextView) findViewById(R.id.txtBreedTypeDetails);
        txtVaccinatedDetails = (TextView) findViewById(R.id.txtVaccinatedDetails);
        txtContactShelterDetails = (TextView) findViewById(R.id.txtContactShelterDetails);
        txtContactAddressDetails = (TextView) findViewById(R.id.txtContactAddressDetails);
        txtContactPhoneDetails = (TextView) findViewById(R.id.txtContactPhoneDetails);
        txtContactWebsiteDetails = (TextView) findViewById(R.id.txtContactWebsiteDetails);
        txtContactInfoDetails = (TextView) findViewById(R.id.txtContactInfoDetails);
        ivFav = (ImageView) findViewById(R.id.iv_favourite);
        viewOne = findViewById(R.id.viewOne);
        viewTwo = findViewById(R.id.viewTwo);
        viewThree = findViewById(R.id.viewThree);
        ivDial = (ImageView) findViewById(R.id.imgContactPhone);
        ivWeb = (ImageView) findViewById(R.id.imgContactWebsite);
        ivEmail = (ImageView) findViewById(R.id.imgContactEmail);

        // hide until its title is clicked
        viewOne.setVisibility(View.GONE);
        // hide until its title is clicked
        viewTwo.setVisibility(View.GONE);
        // hide until its title is clicked
        viewThree.setVisibility(View.GONE);

        ivFav.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mPetIndividualInfo != null) {

                        CheckConnectivity connectivity=new CheckConnectivity(PetIndividualInfoActivity.this);
                        if(connectivity.isConnected())
                            new setFavouriteTask().execute();
                        else Toast.makeText(PetIndividualInfoActivity.this,"Check your internet connection !",Toast.LENGTH_SHORT).show();

                }

            }

        });

        ((LinearLayout) findViewById(R.id.btnAdoptMeTwo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPetIndividualInfo != null) {
                    Intent i = new Intent(PetIndividualInfoActivity.this, MessageToPets.class);
                    i.putExtra("pets_info", mPetIndividualInfo);
                    startActivity(i);
                }
            }
        });
        ((LinearLayout) findViewById(R.id.ll_adopt_me)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPetIndividualInfo != null) {
                    Intent i = new Intent(PetIndividualInfoActivity.this, MessageToPets.class);
                    i.putExtra("pets_info", mPetIndividualInfo);
                    startActivity(i);
                }
            }
        });

        ivDial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               new CallDialBrowserIntents(getParent()).openDialar(mPetIndividualInfo.contact_phone);
            }
        });
        ivWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CallDialBrowserIntents(getParent()).openInBrowser(mPetIndividualInfo.contact_web);
            }
        });
        ivEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CallDialBrowserIntents(getParent()).openEmailApp(mPetIndividualInfo.contact_email);
            }
        });

    }

    private void initMap(){
        FragmentManager fm = getSupportFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.ll_map_container_pets, mapFragment).commit();
            mapFragment.getMapAsync(this);
        }
    }

    private LatLng getLocationPoint(String locationName){

        LatLng mLatLng = null;
        final int maxResults = 5;
        List<Address> locationList;
        double lat = 0;
        double lng = 0;
        try {

            locationList = geocoder.getFromLocationName(locationName, maxResults);
            List<Address> addressList = geocoder.getFromLocationName(
                    locationName, 5);
            if (addressList != null && addressList.size() > 0) {
                lat = addressList.get(0).getLatitude();
                lng = addressList.get(0).getLongitude();
                mLatLng = new LatLng(lat,lng);
            }
            if (locationList == null) {
                Toast.makeText(getApplicationContext(),
                        "No location found",
                        Toast.LENGTH_LONG).show();
            } else {
                if (locationList.isEmpty()) {
                    Toast.makeText(getApplicationContext(),
                            "No location found",
                            Toast.LENGTH_LONG).show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            mLatLng = new LatLng(0,0);
        }

        return mLatLng;
    }


    public void toggle_contents(View v) {
        ImageView imageOne = (ImageView) findViewById(R.id.imgAdd);
        if (viewOne.isShown()) {
            ExpandableView.slide_up(this, viewOne);
            viewOne.setVisibility(View.GONE);

            imageOne.setBackgroundResource((R.drawable.add));
        } else {
            viewOne.setVisibility(View.VISIBLE);
            ExpandableView.slide_down(this, viewOne);

            imageOne.setBackgroundResource((R.drawable.minus));
        }
    }

    public void toggle_contents_two(View v) {
        ImageView imageTwo = (ImageView) findViewById(R.id.imgAddTwo);
        if (viewTwo.isShown()) {
            ExpandableView.slide_up(this, viewTwo);
            viewTwo.setVisibility(View.GONE);

            imageTwo.setBackgroundResource((R.drawable.add));
        } else {
            viewTwo.setVisibility(View.VISIBLE);
            ExpandableView.slide_down(this, viewTwo);

            imageTwo.setBackgroundResource((R.drawable.minus));
        }
    }

    public void toggle_contents_three(View v) {
        ImageView imageThree = (ImageView) findViewById(R.id.imgAddThree);
        if (viewThree.isShown()) {
            ExpandableView.slide_up(this, viewThree);
            viewThree.setVisibility(View.GONE);

            imageThree.setBackgroundResource((R.drawable.add));
        } else {
            viewThree.setVisibility(View.VISIBLE);
            ExpandableView.slide_down(this, viewThree);

            imageThree.setBackgroundResource((R.drawable.minus));
        }
    }

    public void onClickEnlarge(View view) {

        Intent i = new Intent(this, PetIndividualView.class).putExtra("position", viewPager.getCurrentItem()
        ).putExtra("info_pets", mPetIndividualInfo);
        startActivity(i);

    }

    public void onClickBack(View view) {
        finish();
    }

    private void updateUI(PetIndividualInfo petIndividualInfo) {

        txtPetName.setText(petIndividualInfo.title);
//        txtHeaders.setText("Pet Details");
        txtPetBreed.setText(petIndividualInfo.breed_type);
        txtPetLocation.setText(petIndividualInfo.location);
        txtContentPets.setText(petIndividualInfo.about);
        txtLocationDetails.setText(petIndividualInfo.location);
        txtShelterDetails.setText(petIndividualInfo.shelter);
        txtPetTypeDetails.setText(petIndividualInfo.type);
        txtGenderDetails.setText(petIndividualInfo.gender);
        txtAgeRangeDetails.setText(petIndividualInfo.age_range);
        txtSizeTypeDetails.setText(petIndividualInfo.size_type);
        txtBreedTypeDetails.setText(petIndividualInfo.breed_type);
        txtVaccinatedDetails.setText(petIndividualInfo.vaccinated);
        txtContactShelterDetails.setText(petIndividualInfo.shelter);

        txtContactAddressDetails.setText(petIndividualInfo.contact_address);

        txtContactPhoneDetails.setText(petIndividualInfo.contact_phone);

        txtContactWebsiteDetails.setText(petIndividualInfo.contact_web);

        txtContactInfoDetails.setText(petIndividualInfo.contact_email);

        if (petIndividualInfo.favourite == 1) {

            ivFav.setImageResource(R.drawable.heartcolor);

        } else if (petIndividualInfo.favourite == 0) {

            ivFav.setImageResource(R.drawable.heart);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pet_individual, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up ivFav, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onResume() {
        super.onResume();

        if (mapFragment == null && mPetIndividualInfo !=null) {
            mapFragment = SupportMapFragment.newInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.ll_map_container_pets, mapFragment).commit();
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapFragment != null)
            mapFragment.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapFragment != null)

            mapFragment.onLowMemory();
    }



    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(false);
            getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }
    void closeDialog(){
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
        }
        ((FullScreenDialog)prev).dismiss();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng location = null;
        // Add a marker in Sydney, Australia, and move the camera.
        try {
            location = getLocationPoint(mPetIndividualInfo.location);
        }catch (NullPointerException e){
            location = new LatLng(0,0);
            mPetIndividualInfo.location = "No location";
        }
        mMap.addMarker(new MarkerOptions().position(location).title(mPetIndividualInfo.location));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
    }

    private class gePetIndividualInfoTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = new SweetAlertDialog(PetIndividualInfoActivity.this, SweetAlertDialog.PROGRESS_WITHOUT_TEXT_TYPE);
            mDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.primary_color_dark));
            mDialog.setTitleText("");
            mDialog.setCancelable(true);
//            mDialog.show();

            showDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "pets_detail_by_id")
                    .appendQueryParameter("post_id",""+petListInfo.id)
                    .appendQueryParameter("user_id",""+SharedDataSaveLoad.load(PetIndividualInfoActivity.this,getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s))

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {
//                    if (mDialog != null && mDialog.isShowing()) mDialog.dismiss();

                        closeDialog();
                        Toast.makeText(getApplicationContext(), "Request not completed", Toast.LENGTH_SHORT).show();
                    } else {
                        mDialog.dismiss();

                        closeDialog();
                        JSONArray jsonArray = jObj.getJSONArray("pet_detail");

                        for (int i=0; i< jsonArray.length(); i++){

                            mPetIndividualInfo = new PetIndividualInfo();

                            mPetIndividualInfo.id = jsonArray.getJSONObject(i).getInt("id");
                            mPetIndividualInfo.title = jsonArray.getJSONObject(i).getString("title");
                            mPetIndividualInfo.about = jsonArray.getJSONObject(i).getString("about");
                            mPetIndividualInfo.featured_image = jsonArray.getJSONObject(i).getString("featured_image");
                            mPetIndividualInfo.location = jsonArray.getJSONObject(i).getString("location");
                            mPetIndividualInfo.shelter = jsonArray.getJSONObject(i).getString("shelter");
                            mPetIndividualInfo.type = jsonArray.getJSONObject(i).getString("type");
                            mPetIndividualInfo.gender = jsonArray.getJSONObject(i).getString("gender");
                            mPetIndividualInfo.age_range = jsonArray.getJSONObject(i).getString("age_range");
                            mPetIndividualInfo.size_type = jsonArray.getJSONObject(i).getString("size_type");
                            mPetIndividualInfo.breed_type = jsonArray.getJSONObject(i).getString("breed_type");
                            mPetIndividualInfo.vaccinated = jsonArray.getJSONObject(i).getString("vaccinated");
                            mPetIndividualInfo.contact_email = jsonArray.getJSONObject(i).getString("contact_email");
                            mPetIndividualInfo.contact_address = jsonArray.getJSONObject(i).getString("contact_address");
                            mPetIndividualInfo.contact_phone = jsonArray.getJSONObject(i).getString("contact_phone");
                            mPetIndividualInfo.contact_web = jsonArray.getJSONObject(i).getString("contact_web");
                            mPetIndividualInfo.favourite = jsonArray.getJSONObject(i).getInt("favourite");

                            JSONArray jsonImages = jsonArray.getJSONObject(i).getJSONArray("extra_img");
                            List<String> imagesList = new ArrayList<>();
                            for (int k=0; k<jsonImages.length(); k++){

                                imagesList.add(jsonImages.getString(k));
                                mPetIndividualInfo.imageList.add(jsonImages.getString(k));

                            }
                            Log.i("favo", ""+ mPetIndividualInfo.favourite);

                            updateUI(mPetIndividualInfo);
                            swipeAdapterTwo = new SwipeAdapterPetIndividual(getSupportFragmentManager());
                            swipeAdapterTwo.setInfo(mPetIndividualInfo);
                            viewPager.setAdapter(swipeAdapterTwo);
                            indicator.setViewPager(viewPager);

                            /*map integration*/
                            initMap();
                            geocoder = new Geocoder(getApplicationContext(), Locale.ENGLISH);
                        }

                    }
                } catch (JSONException e) {

                    closeDialog();
//                if (mDialog != null && mDialog.isShowing()) mDialog.dismiss();
                    Toast.makeText(PetIndividualInfoActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
        }

    }

    private class setFavouriteTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showDialog();

        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "favorite_pets")
                    .appendQueryParameter("post_id",""+petListInfo.id)
                    .appendQueryParameter("user_id",""+ SharedDataSaveLoad.load(PetIndividualInfoActivity.this,getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s))

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {
                        mPetIndividualInfo.favourite = 0;
                        ivFav.setImageResource(R.drawable.heart);
                        closeDialog();
                        Toast.makeText(getApplicationContext(), jObj.getString("success_status"), Toast.LENGTH_SHORT).show();
                    } else {
                        mDialog.dismiss();

                        mPetIndividualInfo.favourite = 1;
                        ivFav.setImageResource(R.drawable.heartcolor);
                        closeDialog();
                        Toast.makeText(getApplicationContext(), jObj.getString("success_status"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {

                    closeDialog();
                    Toast.makeText(PetIndividualInfoActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
        }

    }


}
