package com.bscheme.saddam.petsofasia.activity.service;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.SwipeAdapterPetIndividual;
import com.bscheme.saddam.petsofasia.activity.MessageToService;
import com.bscheme.saddam.petsofasia.activity.ServiceIndividualView;
import com.bscheme.saddam.petsofasia.fragment.service_provider.LocateFragment;
import com.bscheme.saddam.petsofasia.fragment.service_provider.ServiceAboutFragment;
import com.bscheme.saddam.petsofasia.fragment.service_provider.ServiceContactFragment;
import com.bscheme.saddam.petsofasia.model.PetIndividualInfo;
import com.bscheme.saddam.petsofasia.model.ServiceProviderInfo;
import com.bscheme.saddam.petsofasia.model.ServiceProviderListInfo;
import com.bscheme.saddam.petsofasia.utils.CheckConnectivity;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.CustomPager;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.workarounds.typography.TextView;
import me.relex.circleindicator.CircleIndicator;

public class ServiceProviderInfoActivity extends AppCompatActivity {

    TextView txtHeaders;
    ViewPager viewPagerImage, viewPagerTab;
    MyPagerAdapter myPagerAdapter;

    TabLayout pagerSlidingTabStrip;
    CircleIndicator indicator;

    SwipeAdapterPetIndividual swipeAdapterImage;
    ServiceProviderInfo mSProviderInfo;
    ServiceProviderListInfo mServiceProviderListInfo;

    ImageView ivFav;

    private int[] tabIcons = {
            R.drawable.ic_tab_about,
            R.drawable.ic_tab_contact,
            R.drawable.ic_tab_locate,
            R.drawable.ic_tab_message
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_individual);

        mServiceProviderListInfo = getIntent().getParcelableExtra("info_serv");

        txtHeaders = (TextView) findViewById(R.id.txtHeaders);
        viewPagerImage = (ViewPager) findViewById(R.id.view_pager_image);
        indicator = (CircleIndicator) findViewById(R.id.indicator_default);
        viewPagerTab = (ViewPager) findViewById(R.id.view_pager_two);
        ivFav = (ImageView) findViewById(R.id.iv_favourite);

//        mSProviderInfo = new ServiceProviderInfo();
        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        pagerSlidingTabStrip = (TabLayout) findViewById(R.id.tabs);

        CheckConnectivity connectivity = new CheckConnectivity(ServiceProviderInfoActivity.this);
        if(connectivity.isConnected())
            new getServProvoderInfoTask().execute(mServiceProviderListInfo.id);// get provider info task
        else Toast.makeText(ServiceProviderInfoActivity.this,"Check your internet connection !",Toast.LENGTH_SHORT).show();


        /*image slider*/

        /*
        swipeAdapter = new SwipeAdapterCategoryIndividual(getSupportFragmentManager());
        viewPagerImage.setAdapter(swipeAdapter);

        indicator.setViewPager(viewPagerImage);*/

        //PagerSlidingTabStrip pagerSlidingTabStripTwo = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        viewPagerImage.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int mScrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
//                viewPagerTab.scrollTo(viewPagerImage.getScrollX(), viewPagerTab.getScrollY());
            }

            @Override
            public void onPageSelected(final int position) {

            }

            @Override
            public void onPageScrollStateChanged(final int state) {
                mScrollState = state;
                if (state == ViewPager.SCROLL_STATE_IDLE) {
//                    viewPagerTab.setCurrentItem(viewPagerImage.getCurrentItem(), false);
                }
            }
        });

        viewPagerTab.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int mScrollState = ViewPager.SCROLL_STATE_IDLE;

            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
                if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
                    return;
                }
//                viewPagerImage.scrollTo(viewPagerTab.getScrollX(), viewPagerImage.getScrollY());
            }

            @Override
            public void onPageSelected(final int position) {

                if (position == 3)
                    startActivity(new Intent(ServiceProviderInfoActivity.this, MessageToService.class).putExtra("pets_info", mSProviderInfo));
            }

            @Override
            public void onPageScrollStateChanged(final int state) {
                mScrollState = state;
                if (state == ViewPager.SCROLL_STATE_IDLE) {
//                    viewPagerImage.setCurrentItem(viewPagerTab.getCurrentItem(), false);
                }
            }
        });

        ivFav.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mSProviderInfo != null) {
                    CheckConnectivity connectivity=new CheckConnectivity(ServiceProviderInfoActivity.this);
                    if(connectivity.isConnected())
                        new setFavouriteTask().execute(mSProviderInfo.id);
                    else Toast.makeText(ServiceProviderInfoActivity.this, "Check your internet connection !", Toast.LENGTH_SHORT).show();

                }

            }

        });
    }


    private void setupTabIcons() {
        pagerSlidingTabStrip.getTabAt(0).setIcon(tabIcons[0]);
        pagerSlidingTabStrip.getTabAt(1).setIcon(tabIcons[1]);
        pagerSlidingTabStrip.getTabAt(2).setIcon(tabIcons[2]);
        pagerSlidingTabStrip.getTabAt(3).setIcon(tabIcons[3]);
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        private int mCurrentPosition = -1;
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                CustomPager pager = (CustomPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position-1;
                    pager.measureCurrentView(fragment.getView());
                }
            }
        }
        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0: return ServiceAboutFragment.newInstance(mSProviderInfo);
                case 1: return ServiceContactFragment.newInstance(mSProviderInfo);
                case 2: return LocateFragment.newInstance(mSProviderInfo);
//                case 3: return ServiceProviderMessageFragment.newInstance(mSProviderInfo);
                case 3: return new Fragment();
                default: return ServiceAboutFragment.newInstance(mSProviderInfo);
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) { // Set the tab text
            if (position == 0) {
                return "ABOUT";
            }
            if (position == 1) {
                return "CONTACT";
            }
            if (position == 2) {
                return "LOCATE";
            }
            if (position == 3) {
                return "MESSAGE";
            }
            return getPageTitle(position);
        }

    }

    public void onClickEnlarge(View view) {

        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager_image);

        Intent i = new Intent(this, ServiceIndividualView.class).putExtra("positionTwo", viewPager.getCurrentItem());
        startActivity(i);

    }

    public void onClickBack(View view) {
        finish();
    }
    private void updateUI(ServiceProviderInfo serviceProviderInfo) {

        txtHeaders.setText(serviceProviderInfo.title);

        if(serviceProviderInfo.favourite == 1) {

            ivFav.setImageResource(R.drawable.heartcolor);

        } else if(serviceProviderInfo.favourite == 0) {

            ivFav.setImageResource(R.drawable.heart);

        }
    }


    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(false);
            /*getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();*/

             fragment.show(getSupportFragmentManager().beginTransaction(), "dialog");
        }

    }
    void closeDialog(){
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
//            getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
                    ((FullScreenDialog)prev).dismiss();

        }
    }
    private class getServProvoderInfoTask extends AsyncTask<Integer,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showDialog();
        }

        @Override
        protected String doInBackground(Integer... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "service_detail_by_id")
                    .appendQueryParameter("post_id",""+params[0])
                    .appendQueryParameter("user_id",""+ SharedDataSaveLoad.load(ServiceProviderInfoActivity.this, getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s)) {

                Log.i("s p",s);
                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {

                        closeDialog();
                        Toast.makeText(getApplicationContext(), "Request not completed", Toast.LENGTH_SHORT).show();
                    } else {

                        closeDialog();
                        JSONArray jsonArray = jObj.getJSONArray("pet_detail");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            mSProviderInfo = new ServiceProviderInfo();

                            mSProviderInfo.id = jsonArray.getJSONObject(i).getInt("id");
                            mSProviderInfo.title = jsonArray.getJSONObject(i).getString("title");
                            mSProviderInfo.about = jsonArray.getJSONObject(i).getString("about");
                            mSProviderInfo.featured_image = jsonArray.getJSONObject(i).getString("featured_image");
                            mSProviderInfo.instagramLink = jsonArray.getJSONObject(i).getString("instragram");
                            mSProviderInfo.twitterLink = jsonArray.getJSONObject(i).getString("twitter");
                            mSProviderInfo.facebookLink = jsonArray.getJSONObject(i).getString("facebook");
                            mSProviderInfo.contact_email = jsonArray.getJSONObject(i).getString("contact_email");
                            mSProviderInfo.contact_address = jsonArray.getJSONObject(i).getString("contact_address");
                            mSProviderInfo.contact_phone = jsonArray.getJSONObject(i).getString("contact_phone");
                            mSProviderInfo.contact_web = jsonArray.getJSONObject(i).getString("contact_web");
                            mSProviderInfo.favourite = jsonArray.getJSONObject(i).getInt("favourite");

//                            mSProviderInfo.other_services = jsonArray.getJSONObject(i).getString("other_service");

                            JSONArray jsonImages = jsonArray.getJSONObject(i).getJSONArray("extra_img");
                            List<String> imagesList = new ArrayList<>();
                            for (int k = 0; k < jsonImages.length(); k++) {

                                imagesList.add(jsonImages.getString(k));
                                mSProviderInfo.imageList.add(jsonImages.getString(k));

                            }

                            JSONArray jsonServCat = jsonArray.getJSONObject(i).getJSONArray("services_category");
                            List<String> catlist = new ArrayList<>();
                            for (int k = 0; k < jsonServCat.length(); k++) {

                                catlist.add(jsonServCat.getString(k));
                                mSProviderInfo.serviceCatList.add(jsonServCat.getJSONObject(k).getString("service_cat_name"));

                            }
                            Log.i("favo", "" + mSProviderInfo.favourite);

                            updateUI(mSProviderInfo);
                            swipeAdapterImage = new SwipeAdapterPetIndividual(getSupportFragmentManager());

                            PetIndividualInfo info = new PetIndividualInfo();
                            info.imageList.addAll(mSProviderInfo.imageList);
                            swipeAdapterImage.setInfo(info);

                            viewPagerImage.setAdapter(swipeAdapterImage);
                            indicator.setViewPager(viewPagerImage);

                            if (mSProviderInfo.favourite == 1) {

                                ivFav.setImageResource(R.drawable.heartcolor);

                            } else if (mSProviderInfo.favourite == 0) {

                                ivFav.setImageResource(R.drawable.heart);

                            }
                            viewPagerTab.setAdapter(myPagerAdapter);
                            pagerSlidingTabStrip.setupWithViewPager(viewPagerTab);
                            setupTabIcons();
                        }

                    }
                } catch (JSONException e) {

                    closeDialog();
                    Toast.makeText(ServiceProviderInfoActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }

    private class setFavouriteTask extends AsyncTask<Integer,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showDialog();

        }

        @Override
        protected String doInBackground(Integer... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "favorite_service")
                    .appendQueryParameter("post_id",""+ params[0])
                    .appendQueryParameter("user_id",""+ SharedDataSaveLoad.load(ServiceProviderInfoActivity.this,getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            closeDialog();

            if (!TextUtils.isEmpty(s))

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {
                        mSProviderInfo.favourite = 0;
                        ivFav.setImageResource(R.drawable.heart);
                        Toast.makeText(getApplicationContext(), jObj.getString("success_status"), Toast.LENGTH_SHORT).show();
                    } else {

                        mSProviderInfo.favourite = 1;
                        ivFav.setImageResource(R.drawable.heartcolor);
                        Toast.makeText(getApplicationContext(), jObj.getString("success_status"), Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {

                    Toast.makeText(ServiceProviderInfoActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
        }

    }
}
