package com.bscheme.saddam.petsofasia.fragment;


import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.adapter.PetListAdapter;
import com.bscheme.saddam.petsofasia.model.PetListInfo;
import com.bscheme.saddam.petsofasia.utils.CheckConnectivity;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class PetsListFragment extends Fragment {


    RecyclerView mRecyclerView;

    SweetAlertDialog mDialog;
    List<PetListInfo> mPetListInfoList;
    PetListAdapter mAdapter;
    public PetsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pets_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_petList);
        mRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);

        mRecyclerView.setLayoutManager(gridLayoutManager);
        mPetListInfoList = new ArrayList<>();
        mAdapter = new PetListAdapter(getActivity(),mPetListInfoList);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CheckConnectivity connectivity=new CheckConnectivity(getActivity());
        if(connectivity.isConnected())
            new gePetListTask().execute();
        else Toast.makeText(getActivity(),"Check your internet connection !",Toast.LENGTH_SHORT).show();

    }

    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(false);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }
    void closeDialog(){
        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getActivity().getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
//            prev.dismiss();
            Log.i("frag diag" , "close");
        }
    }
    private class gePetListTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_WITHOUT_TEXT_TYPE);
            mDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.primary_color_dark));
            mDialog.setTitleText("");
            mDialog.setCancelable(true);
//            mDialog.show();

            showDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "list_of_all_pets");
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s)) {
                Log.i("res", "" + s);

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {
//                    if (mDialog != null && mDialog.isShowing()) mDialog.dismiss();

                        closeDialog();
                        Toast.makeText(getActivity().getApplicationContext(), "Request not completed", Toast.LENGTH_SHORT).show();
                    } else {
//                    mDialog.dismiss();
                        closeDialog();

                        JSONArray jsonArray = jObj.getJSONArray("all_pets");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            PetListInfo mPetListInfo = new PetListInfo();
                            mPetListInfo.id = jsonArray.getJSONObject(i).getInt("post_id");
                            mPetListInfo.title = jsonArray.getJSONObject(i).getString("title");
                            JSONArray jsonImages = jsonArray.getJSONObject(i).getJSONArray("images");
                            List<String> imagesList = new ArrayList<>();
                            for (int k = 0; k < jsonImages.length(); k++) {

                                imagesList.add(jsonImages.getString(k));

                            }
                            mPetListInfo.imageList.addAll(imagesList);

                            mPetListInfoList.add(mPetListInfo);
                        }

                        mAdapter.notifyDataSetChanged();

                    }
                } catch (JSONException e) {

                    closeDialog();
//                if (mDialog != null && mDialog.isShowing()) mDialog.dismiss();
                    Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }

}
