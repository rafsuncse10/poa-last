package com.bscheme.saddam.petsofasia.activity.service;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.Search;
import com.bscheme.saddam.petsofasia.adapter.SeviceProviderListAdapter;
import com.bscheme.saddam.petsofasia.model.ServiceListInfo;
import com.bscheme.saddam.petsofasia.model.ServiceProviderListInfo;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class ServiceProviderListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    TextView txtActionbarTitle;
    ListView lvSerciveProvider;
    List<ServiceProviderListInfo> mServiceProviderListInfos;

    SeviceProviderListAdapter mAdapter;
    ServiceListInfo mServiceListInfo;

    private GestureDetector mGestureDetector;
//    private List<Object[]> alphabet = new ArrayList<Object[]>();
    private String alphabetStr = "";
    private HashMap<String, Integer> sections = new HashMap<String, Integer>();

    private int sideIndexHeight;
    private static float sideIndexX;
    private static float sideIndexY;
    private int indexListSize;


    class SideIndexGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            sideIndexX = sideIndexX - distanceX;
            sideIndexY = sideIndexY - distanceY;

            if (sideIndexX >= 0 && sideIndexY >= 0) {
                displayListItem();
            }

            return super.onScroll(e1, e2, distanceX, distanceY);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_category);

        mGestureDetector = new GestureDetector(this, new SideIndexGestureListener());


        lvSerciveProvider = (ListView) findViewById(R.id.lv_service_provider);
        txtActionbarTitle = (in.workarounds.typography.TextView) findViewById(R.id.txtHeaders);

        mServiceListInfo = getIntent().getParcelableExtra("info");

        txtActionbarTitle.setText(mServiceListInfo.title);

        mServiceProviderListInfos = new ArrayList<>();

        new geServiceProviderListTask().execute(mServiceListInfo.id);

    }

    public void onClickSearch(View view) {
        Intent i = new Intent(this, Search.class);
        startActivity(i);
    }

    public void onClickBack(View view) {
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        /*ServiceProviderListInfo mServiceProviderListInfo = (ServiceProviderListInfo)parent.getAdapter().getItem(position);
        Intent providerIntent = new Intent(ServiceProviderListActivity.this, ServiceProviderInfoActivity.class);
        startActivity(providerIntent);*/
    }
    @Override
   public boolean onTouchEvent(MotionEvent event) {
       if (mGestureDetector.onTouchEvent(event)) {
           return true;
       } else {
           return false;
       }
   }
    private void creatSideAlphabeticIndex(List<ServiceProviderListInfo> serviceProviderList){

        Collections.sort(serviceProviderList, new Comparator<ServiceProviderListInfo>() {
            @Override
            public int compare(ServiceProviderListInfo lhs, ServiceProviderListInfo rhs) {
                return (lhs.title).compareToIgnoreCase(rhs.title);
            }
        });

        List<SeviceProviderListAdapter.Row> rows = new ArrayList<SeviceProviderListAdapter.Row>();
        int start = 0;
        int end = 0;
        String previousLetter = null;
        Object[] tmpIndexItem = null;
        Pattern numberPattern = Pattern.compile("[0-9]");

        for (ServiceProviderListInfo serviceProviderListInfo : serviceProviderList) {
            String firstLetter = serviceProviderListInfo.title.substring(0, 1);

            // Group numbers together in the scroller
            if (numberPattern.matcher(firstLetter).matches()) {
                firstLetter = "#";
            }

            // If we've changed to a new letter, add the previous letter to the alphabet scroller
            if (previousLetter != null && !firstLetter.equals(previousLetter)) {
                end = rows.size() - 1;
                tmpIndexItem = new Object[3];
                tmpIndexItem[0] = previousLetter.toUpperCase(Locale.US);
                tmpIndexItem[1] = start;
                tmpIndexItem[2] = end;
//                alphabet.add(tmpIndexItem);

                start = end + 1;
            }

            // Check if we need to add a header row
            if (!firstLetter.equals(previousLetter)) {
                rows.add(new SeviceProviderListAdapter.Section(firstLetter));
                sections.put(firstLetter, start);
            }

            // Add the service title to the list
            rows.add(new SeviceProviderListAdapter.Item(serviceProviderListInfo));
            previousLetter = firstLetter;
        }

        if (previousLetter != null) {
            // Save the last letter
            tmpIndexItem = new Object[3];
            tmpIndexItem[0] = previousLetter.toUpperCase(Locale.UK);
            tmpIndexItem[1] = start;
            tmpIndexItem[2] = rows.size() - 1;
//            alphabet.add(tmpIndexItem);
        }

        alphabetStr = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        mAdapter = new SeviceProviderListAdapter(ServiceProviderListActivity.this);
        mAdapter.setRows(rows);
        lvSerciveProvider.setAdapter(mAdapter);

        updateList();

        lvSerciveProvider.setOnItemClickListener(this);
    }
    public void updateList() {
        LinearLayout sideIndex = (LinearLayout) findViewById(R.id.ll_sideIndex);
        sideIndex.removeAllViews();
        indexListSize = alphabetStr.length();
        if (indexListSize < 1) {
            return;
        }

        int indexMaxSize = (int) Math.floor(sideIndex.getHeight() / 20);
        int tmpIndexListSize = indexListSize;
        while (tmpIndexListSize > indexMaxSize) {
            tmpIndexListSize = tmpIndexListSize / 2;
        }
        double delta;
        if (tmpIndexListSize > 0) {
            delta = indexListSize / tmpIndexListSize;
        } else {
            delta = 1;
        }

        TextView tmpTV;
        for (double i = 1; i <= indexListSize; i = i + delta) {
//            Object[] tmpIndexItem = alphabet.get((int) i - 1);
            String tmpLetter = String.valueOf(alphabetStr.charAt((int) i - 1));

            tmpTV = new TextView(this);
            tmpTV.setText(tmpLetter);
            tmpTV.setTextColor(getResources().getColor(R.color.text_color));
            tmpTV.setGravity(Gravity.CENTER);
            tmpTV.setTextSize(15);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
            tmpTV.setLayoutParams(params);
            sideIndex.addView(tmpTV);
        }

        sideIndexHeight = sideIndex.getHeight();

        sideIndex.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // now you know coordinates of touch
                sideIndexX = event.getX();
                sideIndexY = event.getY();

                // and can display a proper item it country list
                displayListItem();

                return false;
            }
        });
    }

    public void displayListItem() {
        LinearLayout sideIndex = (LinearLayout) findViewById(R.id.ll_sideIndex);
        sideIndexHeight = sideIndex.getHeight();
        // compute number of pixels for every side index item
        double pixelPerIndexItem = (double) sideIndexHeight / indexListSize;

        // compute the item index for given event position belongs to
        int itemPosition = (int) (sideIndexY / pixelPerIndexItem);

        // get the item (we can do it since we know item index)
        if (itemPosition < alphabetStr.length()) {
            char indexItem = alphabetStr.charAt(itemPosition);
//            Object[] indexItem = alphabetStr.charAt(itemPosition);
//            int subitemPosition = sections.get(indexItem[0]);

            //ListView listView = (ListView) findViewById(android.R.id.list);
//            lvSerciveProvider.setSelection(subitemPosition);
        }
    }

    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(false);
            getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }
    void closeDialog(){
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
//            prev.dismiss();
            Log.i("frag diag", "close");
        }
    }
    private class geServiceProviderListTask extends AsyncTask<Integer,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showDialog();

        }

        @Override
        protected String doInBackground(Integer... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "get_service_by_category")
                    .appendQueryParameter("cat_id", ""+params[0])
                    .appendQueryParameter("user_id", ""+ SharedDataSaveLoad.load(ServiceProviderListActivity.this,getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            closeDialog();

            if (!TextUtils.isEmpty(s)) {

                Log.i("response servPro" ,s);
                try {

                    JSONArray jsonArray = new JSONArray(s);

                    for (int i = 0; i < jsonArray.length(); i++) {

                        ServiceProviderListInfo mServiceProviderListInfo = new ServiceProviderListInfo();

                        mServiceProviderListInfo.id = jsonArray.getJSONObject(i).getInt("service_id");
                        mServiceProviderListInfo.title = jsonArray.getJSONObject(i).getString("service_title");
                        mServiceProviderListInfo.imgLink = jsonArray.getJSONObject(i).getString("image");
                        mServiceProviderListInfo.location = jsonArray.getJSONObject(i).getString("location");

                        JSONArray jsonServCat = jsonArray.getJSONObject(i).getJSONArray("service_cat");
                        for (int k = 0; k < jsonServCat.length(); k++) {

                            mServiceProviderListInfo.serviceList.add(jsonServCat.getJSONObject(k).getString("service_cat_name"));

                        }
                        mServiceProviderListInfos.add(mServiceProviderListInfo);
                    }

                    creatSideAlphabeticIndex(mServiceProviderListInfos);

                } catch (JSONException e) {
                    Toast.makeText(ServiceProviderListActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }
}
