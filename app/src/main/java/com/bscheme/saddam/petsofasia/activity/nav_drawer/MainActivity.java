package com.bscheme.saddam.petsofasia.activity.nav_drawer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.Search;
import com.bscheme.saddam.petsofasia.fragment.fragment_left_menu.HomeFragment;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

public class MainActivity extends SlidingFragmentActivity {

    TextView toolBarTitle;
    Fragment mContent;
    String titleText = null ;

    SlidingMenu sm;
    ImageView ivSearch,menu;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
//        getSupportActionBar().hide();

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        getSlidingMenu().setMode(SlidingMenu.LEFT);
        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

        setContentView(R.layout.content_frame);

        final Toolbar customToolbar = (Toolbar) this.findViewById(R.id.custom_toolbar);
        customToolbar.setTitle("");
        setSupportActionBar(customToolbar);
        ivSearch = (ImageView) customToolbar.findViewById(R.id.img_search);
        menu = (ImageView) customToolbar.findViewById(R.id.iv_nav_home);
//        customToolbar.setNavigationIcon(R.drawable.menu);

        // set the Behind View
        setBehindContentView(R.layout.left_menu_frame);

        if (savedInstanceState == null) {
            FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
            t.replace(R.id.menu_frame_left, new LeftSideFragment());
            t.commit();
        }
        else {
            getSupportFragmentManager().findFragmentById(R.id.menu_frame_left);
        }


        // customize the SlidingMenu
        sm = getSlidingMenu();
        sm.setShadowWidthRes(R.dimen.shadow_width);
        sm.setShadowDrawable(R.drawable.shadow);
        sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        sm.setFadeDegree(0.35f);
        sm.setFadeEnabled(true);
        setSlidingActionBarEnabled(true);


        // set the Above View Fragment for home content
        if (savedInstanceState != null)
        {
            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
        }
        if (mContent == null)
        {
            titleText = "Kindividual";
            mContent = new HomeFragment();
            setHomeView();

        }

        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler hh=new Handler();
                hh.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(MainActivity.this, Search.class);
                        startActivity(i);
                    }
                },100);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Handler hh=new Handler();
                        hh.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getSlidingMenu().toggle();
                            }
                        },100);
                    }
                });


    }

    private void setHomeView()
    {
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, mContent).commit();
//        toolBarTitle.setText(titleText);
    }


    public void setToolBarTitle(String title) {
        this.titleText = title;
        if (toolBarTitle != null)    toolBarTitle.setText(titleText);
    }

    public void setVisibility(boolean isVisible){
        if (isVisible)
            ivSearch.setVisibility(View.VISIBLE);
        else
            ivSearch.setVisibility(View.GONE);
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "mContent", mContent);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    public void switchContainerFragment(Fragment fragment){
        mContent = fragment;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            public void run() {
                getSlidingMenu().showContent();
            }
        }, 100);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Handler hh=new Handler();
                hh.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        getSlidingMenu().toggle();

                    }
                },100);
                break;
            /*case R.id.action_search:
                hh=new Handler();
                hh.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(MainActivity.this, Search.class);
                        startActivity(i);
                    }
                },100);
                break;*/
        }
        return true;
    }

}
