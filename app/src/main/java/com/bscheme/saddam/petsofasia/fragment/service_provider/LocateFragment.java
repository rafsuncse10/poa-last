package com.bscheme.saddam.petsofasia.fragment.service_provider;

/* solved map in nested fragment issue
* http://stackoverflow.com/questions/18206615/how-to-use-google-map-v2-inside-fragment
* */
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.model.ServiceProviderInfo;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocateFragment extends Fragment implements OnMapReadyCallback {

    ServiceProviderInfo mProviderInfo;
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    Geocoder geocoder = null;

    public LocateFragment() {
        // Required empty public constructor
    }
    public static LocateFragment newInstance(ServiceProviderInfo providerInfo) {

        LocateFragment f = new LocateFragment();
        Bundle b = new Bundle();
        b.putParcelable("info_provider", providerInfo);

        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProviderInfo = (ServiceProviderInfo) getArguments().getParcelable("info_provider");
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_locate, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initMap();
        geocoder = new Geocoder(getActivity().getApplicationContext(), Locale.ENGLISH);

    }

    private void initMap(){
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map_container, mapFragment).commit();
            mapFragment.getMapAsync(this);
        }
    }

    private LatLng getLocationPoint(String locationName){

        final int maxResults = 5;
        List<Address> locationList;
        double lat = 0;
        double lng = 0;
        try {

            locationList = geocoder.getFromLocationName(locationName, maxResults);
            List<Address> addressList = geocoder.getFromLocationName(
                    locationName, 5);
            if (addressList != null && addressList.size() > 0) {
                lat = addressList.get(0).getLatitude();
                lng = addressList.get(0).getLongitude();
            }
            if (locationList == null) {
                Toast.makeText(getActivity().getApplicationContext(),
                        "No location found",
                        Toast.LENGTH_LONG).show();
            } else {
                if (locationList.isEmpty()) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "No location found",
                            Toast.LENGTH_LONG).show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return new LatLng(lat,lng);
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.i("map", "ready");
        mMap = googleMap;


        // Add a marker in Sydney, Australia, and move the camera.
        LatLng location = getLocationPoint(mProviderInfo.contact_address);

        mMap.addMarker(new MarkerOptions().position(location).title(mProviderInfo.contact_address));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            getChildFragmentManager().beginTransaction().replace(R.id.map_container, mapFragment).commit();
            mapFragment.getMapAsync(this);
        }
//        initMap();
//        mapFragment.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mapFragment != null)
        mapFragment.onPause();
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mapFragment != null)

            mapFragment.onDestroyView();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapFragment != null)

            mapFragment.onLowMemory();
    }

    /**
     * http://stackoverflow.com/questions/18206615/how-to-use-google-map-v2-inside-fragment
     * */
    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class
                    .getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
