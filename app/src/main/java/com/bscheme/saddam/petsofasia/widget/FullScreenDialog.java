package com.bscheme.saddam.petsofasia.widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;

import com.bscheme.petsofasia.R;

/**
 * Created by Bscheme on 2/7/16.
 */
public class FullScreenDialog extends DialogFragment {

    private ImageView ivLoading;
    int width, height;
    @Override
    public void onStart() {
        super.onStart();
        WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);

        ViewGroup.LayoutParams params = null;

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
            width = manager.getDefaultDisplay().getWidth();
            height = manager.getDefaultDisplay().getHeight();
        } else {
            Point point = new Point();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            manager.getDefaultDisplay().getMetrics(displayMetrics);
            width = displayMetrics.widthPixels;
            height = displayMetrics.heightPixels;
        }

        Dialog d = getDialog();
        if (d!=null){
//            int width = ViewGroup.LayoutParams.MATCH_PARENT;
//            int height = ViewGroup.LayoutParams.MATCH_PARENT;

            d.getWindow().setLayout(width,height);
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dialog dialog = getDialog();
        if (dialog != null) {
//            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            AnimationDrawable loadingAnimation = (AnimationDrawable) ivLoading.getBackground();
            loadingAnimation.start();
        }
        setStyle(DialogFragment.STYLE_NO_TITLE,android.R.style.Theme_NoTitleBar_Fullscreen);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.loading_page, container, false);
        if (view != null) {
            ivLoading = (ImageView) view.findViewById(R.id.iv_loading);
            ivLoading.setBackgroundResource(R.drawable.loading);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        WebView mweb = (WebView) view.findViewById(R.id.webLoader);
//        mweb.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mweb.loadUrl("file:///android_asset/loading.gif");
    }
}
