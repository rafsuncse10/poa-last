package com.bscheme.saddam.petsofasia.fragment;


import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.adapter.ServiceListAdapter;
import com.bscheme.saddam.petsofasia.model.ServiceListInfo;
import com.bscheme.saddam.petsofasia.utils.CheckConnectivity;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */

public class ServicesListFragment extends Fragment {

    RecyclerView mRecyclerView;

    SweetAlertDialog mDialog;
    List<ServiceListInfo> mServiceListInfoList;
    ServiceListAdapter mAdapter;

    public ServicesListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_services_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_serviceList);
        mRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);

        mRecyclerView.setLayoutManager(gridLayoutManager);
        mServiceListInfoList = new ArrayList<>();
        mAdapter = new ServiceListAdapter(getActivity(),mServiceListInfoList);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CheckConnectivity connectivity=new CheckConnectivity(getActivity());

        if(connectivity.isConnected())
            new geServiceListTask().execute();
        else Toast.makeText(getActivity(),"Check your internet connection !",Toast.LENGTH_SHORT).show();

    }

    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(true);

            fragment.show(getActivity().getSupportFragmentManager().beginTransaction(), "dialog");
        }

    }
    void closeDialog(){
        FullScreenDialog prev = (FullScreenDialog) getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
//            getActivity().getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
            prev.dismiss();
            Log.i("frag diag" , "close");
        }

    }
    private class geServiceListTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_WITHOUT_TEXT_TYPE);
            mDialog.getProgressHelper().setBarColor(getResources().getColor(R.color.primary_color_dark));
            mDialog.setTitleText("");
            mDialog.setCancelable(true);
//            mDialog.show();

            showDialog();

        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "list_of_all_service_category");
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            closeDialog();

            if (!TextUtils.isEmpty(s))

            try {

                    JSONArray jsonArray = new JSONArray(s);

                    for (int i=0; i< jsonArray.length(); i++){

                        ServiceListInfo mPetListInfo = new ServiceListInfo();
                        mPetListInfo.id = jsonArray.getJSONObject(i).getInt("id");
                        mPetListInfo.title = jsonArray.getJSONObject(i).getString("name");
                        mPetListInfo.imgLink = jsonArray.getJSONObject(i).getString("icon");

                        mServiceListInfoList.add(mPetListInfo);
                    }

                    mAdapter.notifyDataSetChanged();


            } catch (JSONException e) {
                Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

    }

}
