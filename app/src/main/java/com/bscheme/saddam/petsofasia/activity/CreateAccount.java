package com.bscheme.saddam.petsofasia.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.activity.nav_drawer.MainActivity;
import com.bscheme.saddam.petsofasia.utils.CheckConnectivity;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import in.workarounds.typography.EditText;
import in.workarounds.typography.TextView;

public class CreateAccount extends AppCompatActivity {

    EditText edt_email,edtPassword,edtFullName;
    LinearLayout btnSignUp,signupFb;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*Facebook sdk initialize*/
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_create_account);

        TextView existingAccount = (TextView) findViewById(R.id.alreadyHaveAnAccount);
        existingAccount.setText(Html.fromHtml("Already have an <u>Account?</u>"));

        edt_email = (EditText) findViewById(R.id.emailAddress);
        edtPassword = (EditText) findViewById(R.id.password);
        edtFullName = (EditText) findViewById(R.id.fullName);
        btnSignUp = (LinearLayout) findViewById(R.id.btn_signUp);
        signupFb = (LinearLayout) findViewById(R.id.registerUsingFacebook);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edt_email.getText().toString()) || TextUtils.isEmpty(edtFullName.getText().toString()) || TextUtils.isEmpty(edtPassword.getText().toString())){
                    Toast.makeText(CreateAccount.this,"Please fill all field",Toast.LENGTH_SHORT).show();
                }else {
                    CheckConnectivity connectivity=new CheckConnectivity(CreateAccount.this);

                    if(connectivity.isConnected())
                        new getRes().execute(edt_email.getText().toString(),edtFullName.getText().toString(),edtPassword.getText().toString(),"app");
                    else Toast.makeText(CreateAccount.this,"Check your internet connection !",Toast.LENGTH_SHORT).show();

                }
            }
        });
        existingAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CreateAccount.this, LogIn.class);
                startActivity(i);
            }
        });
        signupFb.setOnClickListener(fbClickListener);

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, first_name, last_name,name, email, birthday, location");
                        /* make the API call */
                        new GraphRequest(
                                AccessToken.getCurrentAccessToken(),
                                "/" + loginResult.getAccessToken().getUserId(),
                                parameters,
                                HttpMethod.GET,
                                new GraphRequest.Callback() {
                                    public void onCompleted(GraphResponse response) {
                                        /* handle the result */
                                        Log.i("graph", "" + response.getJSONObject());
                                        JSONObject object = response.getJSONObject();


                                        String name = null, email = null, id;
                                        try {
                                            if (object.has("first_name"))

                                                if (object.has("last_name"))
                                                    if (object.has("email"))
                                                        email = object.getString("email");
                                            if (object.has("name"))
                                                name = object.getString("name");
                                            CheckConnectivity connectivity=new CheckConnectivity(CreateAccount.this);

                                            if(connectivity.isConnected())
                                                new getRes().execute(email, name, "", "fb");
                                            else Toast.makeText(CreateAccount.this,"Check your internet connection !",Toast.LENGTH_SHORT).show();


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }).executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(CreateAccount.this, "User declined ", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(CreateAccount.this, "" + exception.toString(), Toast.LENGTH_LONG).show();
                    }
                });

        ((TextView)findViewById(R.id.term_condition)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CreateAccount.this, TermsConditionActivity.class);
                startActivity(i);
            }
        });

    }


    private View.OnClickListener fbClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LoginManager.getInstance().logInWithReadPermissions(CreateAccount.this, Arrays.asList("public_profile", "email"));

        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(true);
            getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }
    void closeDialog(){
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
        }
        ((FullScreenDialog)prev).dismiss();
    }

    private class getRes extends AsyncTask<String,Void,String> {

        Handler h;
        private static final int DELAY=3000; //milis

        @Override
        protected void onPreExecute() {
            h = new Handler();
            h.postDelayed(new Runnable() {
                public void run() {
                    if (h != null)
                        h.removeCallbacksAndMessages(null);
                    cancelAfterTimeout(true);
                }
            }, DELAY);

            showDialog();

            super.onPreExecute();

        }
        private void cancelAfterTimeout(boolean interrupt){
            //cancel/force interrupt/dissable/remove connection here
            //which you are initializing in doInBackground
            cancel(interrupt);
        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action","user_registration")
                    .appendQueryParameter("email", params[0])
                    .appendQueryParameter("password", params[2])
                    .appendQueryParameter("full_name",params[1])
                    .appendQueryParameter("source",params[3]);
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            closeDialog();
            if(h!=null)
                h.removeCallbacksAndMessages(null);

            if (!TextUtils.isEmpty(s)) {

                Log.i("resp", s);
                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {
                        Toast.makeText(getApplicationContext(), "" + jObj.getString("success_status"), Toast.LENGTH_SHORT).show();
                    } else {
                        if (jObj.has("user_id"))
                            SharedDataSaveLoad.save(CreateAccount.this, getResources().getString(R.string.shared_pref_user_id), jObj.getString("user_id"));
                        if (jObj.has("user_email")) {
                            Intent intent = new Intent(CreateAccount.this, MainActivity.class);
                            startActivity(intent);
                            SharedDataSaveLoad.save(CreateAccount.this, getResources().getString(R.string.shared_pref_user_email), jObj.getString("user_email"));
                            setResult(1);
                            CreateAccount.this.finish();

                        }
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }


    }

}
