package com.bscheme.saddam.petsofasia.activity.nav_drawer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bscheme.saddam.petsofasia.fragment.fragment_left_menu.AboutUsFragment;
import com.bscheme.saddam.petsofasia.activity.LogIn;
import com.bscheme.saddam.petsofasia.fragment.fragment_left_menu.PetsFavouriteFragment;
import com.bscheme.saddam.petsofasia.fragment.fragment_left_menu.PrivacyPolicyFragment;
import com.bscheme.saddam.petsofasia.fragment.fragment_left_menu.ProfileFragment;
import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.fragment.fragment_left_menu.ServicesFavouriteFragment;
import com.bscheme.saddam.petsofasia.fragment.fragment_left_menu.TermsConditionsFragment;
import com.bscheme.saddam.petsofasia.fragment.fragment_left_menu.HomeFragment;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;

import org.codechimp.apprater.AppRater;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LeftSideFragment extends Fragment implements View.OnClickListener {

    TextView tvFullName,logout;
    ImageView ivProfileImg,imgArrow;

    private static final int HIDE_THRESHOLD = 20;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;

    private String cat,post_id;

    LinearLayout llPets,llHome,llService,llProfile,llTNC,llPrivacy,llAboutUs,llRateUs,llLogOut;

    public LeftSideFragment newInstance(String cat, String post_id) {
        LeftSideFragment f = new LeftSideFragment();
        Bundle args = new Bundle();
        args.putString("cat",cat);
        args.putString("post",post_id);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cat = getArguments().getString("cat");
            post_id = getArguments().getString("post");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_left_drawer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        llPets = (LinearLayout) view.findViewById(R.id.petsLayout);
        llHome = (LinearLayout) view.findViewById(R.id.ll_home);
        llService = (LinearLayout) view.findViewById(R.id.servicesLayout);
        llProfile = (LinearLayout) view.findViewById(R.id.profileLayout);
        llTNC = (LinearLayout) view.findViewById(R.id.tncLayout);
        llPrivacy = (LinearLayout) view.findViewById(R.id.privacyLayout);
        llAboutUs = (LinearLayout) view.findViewById(R.id.aboutUsLayout);
        llRateUs = (LinearLayout) view.findViewById(R.id.rateUsLayout);
        llLogOut = (LinearLayout) view.findViewById(R.id.logOutLayout);


    }


    @Override
    public void onActivityCreated(@Nullable final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        llPets.setOnClickListener(this);
        llHome.setOnClickListener(this);
        llService.setOnClickListener(this);
        llTNC.setOnClickListener(this);
        llPrivacy.setOnClickListener(this);
        llAboutUs.setOnClickListener(this);
        llRateUs.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llLogOut.setOnClickListener(this);


    }

    //switch fragment on left menu list clicked
    public void switchFragment(Fragment fragment,String title,boolean visibility) {
        if (getActivity() == null) {
            return;
        }

        MainActivity home = (MainActivity) getActivity();
        home.switchContainerFragment(fragment);
        home.setToolBarTitle(title);
        home.setVisibility(visibility);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_home:
                switchFragment(new HomeFragment(),"",true);

                break;
            case R.id.petsLayout:
                switchFragment(new PetsFavouriteFragment(),"",false);
                setHasOptionsMenu(false);
                break;
            case R.id.servicesLayout:
                switchFragment(new ServicesFavouriteFragment(), "",false);
                setHasOptionsMenu(false);

                break;
            case R.id.profileLayout:
                switchFragment(new ProfileFragment(), "",false);
                setHasOptionsMenu(false);

                break;
            case R.id.tncLayout:
                switchFragment(new TermsConditionsFragment(), "",false);
                setHasOptionsMenu(false);

                break;
            case R.id.privacyLayout:
                switchFragment(new PrivacyPolicyFragment(), "",false);
                break;
            case R.id.aboutUsLayout:
                switchFragment(new AboutUsFragment(), "",false);
                break;
            case R.id.rateUsLayout:
                AppRater.app_launched(getActivity());

                AppRater.showRateDialog(v.getContext());
                break;
            case R.id.logOutLayout:

                SweetAlertDialog sDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_MODIFIED_TYPE);
                sDialog.setTitleText("Want to logout");
                sDialog.setConfirmText("Yes");
                sDialog.setCancelText("No");
                sDialog.setCancelable(false);
                sDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    public void onClick(SweetAlertDialog mDialog) {
                        SharedDataSaveLoad.remove(getActivity(), getResources().getString(R.string.shared_pref_user_id));
                        getActivity().finish();
                        startActivity(new Intent(getActivity(), LogIn.class));
                        mDialog.dismiss();

                    }
                });
                sDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    public void onClick(SweetAlertDialog mDialog) {
                        mDialog.dismiss();
                    }
                });
                sDialog.show();

                break;
        }
    }
}
