package com.bscheme.saddam.petsofasia.fragment.service_provider;


import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.activity.MessageToService;
import com.bscheme.saddam.petsofasia.model.ServiceProviderInfo;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenConfirmationDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import in.workarounds.typography.Button;
import in.workarounds.typography.EditText;
import in.workarounds.typography.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceProviderMessageFragment extends Fragment {

    TextView txtHeaders,petName;
    Button btnSend;
    ImageView pet_img;
    EditText edtMsg;
    ServiceProviderInfo mProviderInfo;

    public ServiceProviderMessageFragment() {
        // Required empty public constructor
    }

    public static ServiceProviderMessageFragment newInstance(ServiceProviderInfo providerInfo) {

        ServiceProviderMessageFragment f = new ServiceProviderMessageFragment();
        Bundle b = new Bundle();
        b.putParcelable("info_provider", providerInfo);

        f.setArguments(b);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_service_provider_message, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mProviderInfo = (ServiceProviderInfo) getArguments().getParcelable("info_provider");
        }

        txtHeaders = (TextView) view.findViewById(R.id.txtHeaders);
        petName = (TextView) view.findViewById(R.id.txt_pet_name);
        btnSend = (Button) view.findViewById(R.id.btn_send);
        pet_img = (ImageView) view.findViewById(R.id.iv_pet_img);
        edtMsg = (EditText) view.findViewById(R.id.edt_message);

        petName.setText(mProviderInfo.title);

        Picasso.with(getActivity()).load(""+mProviderInfo.featured_image).into(pet_img);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(MessageToPets.this, Confirmation.class);
//                startActivity(i);
                if (!TextUtils.isEmpty(edtMsg.getText().toString())) {
                    new sendEmailTask().execute(mProviderInfo.contact_email,"Abopt "+mProviderInfo.title.toUpperCase(Locale.ENGLISH),edtMsg.getText().toString());
                } else {
                    Toast.makeText(getActivity(), "Please write a msg !!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().startActivity(new Intent(getActivity(), MessageToService.class).putExtra("pets_info", mProviderInfo));
    }

    void showDialog() {

        FullScreenConfirmationDialog fragment = (FullScreenConfirmationDialog) getActivity().getSupportFragmentManager().findFragmentByTag("dialog_confirmation");
        if (fragment == null) {
            fragment = new FullScreenConfirmationDialog();
            fragment.setCancelable(false);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog_confirmation")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }
    void closeDialog(){
        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog_confirmation");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getActivity().getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
        }
//        ((FullScreenConfirmationDialog)prev).dismiss();
    }

    private class sendEmailTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "send_email")
                    .appendQueryParameter("to_mail", "" + params[0])
                    .appendQueryParameter("from_mail", ""+SharedDataSaveLoad.load(getActivity(), getActivity().getResources().getString(R.string.shared_pref_user_email)))
                    .appendQueryParameter("subject", ""+params[1])
                    .appendQueryParameter("message", ""+params[2]);
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s)) {

                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {
                        Toast.makeText(getActivity(), "" + jObj.getString("success_status"), Toast.LENGTH_SHORT).show();
                    } else {

                        showDialog();

                    }
                } catch (JSONException e) {

                    Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }


}
