package com.bscheme.saddam.petsofasia.utils;

import android.net.Uri;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by root on 11/27/14.
 */
public class ConnectionHelper {

    private HttpURLConnection httpURLConnection;
    private DataOutputStream dos;

    public void createConnection(String mUrl,String mRequestType){

        try {
            httpURLConnection = (HttpURLConnection) (new URL(mUrl)).openConnection();
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod(mRequestType);

            httpURLConnection.connect();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void addData(Uri.Builder builder){
        try {

            String query = builder.build().getEncodedQuery();

            dos = new DataOutputStream(httpURLConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(dos, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            dos.close();

//            httpURLConnection.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String getResponse(){
        InputStream is = null;
        StringBuilder buffer = null;
        try {
            is = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            buffer = new StringBuilder();

            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line + "\n");
            }
            return buffer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            httpURLConnection.disconnect();
        }

        return null;
    }

}
