package com.bscheme.saddam.petsofasia.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bscheme.petsofasia.R;

import in.workarounds.typography.TextView;

public class ForgotPassword extends AppCompatActivity {

    LinearLayout btnResetPass;
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        back = (ImageView) findViewById(R.id.imgBack);
        TextView forgotPassword = (TextView) findViewById(R.id.forgotPassword);
        btnResetPass = (LinearLayout) findViewById(R.id.resetPassword);
        forgotPassword.setText(Html.fromHtml("<b>Forgot your Password?</b>" +
                "<br>Type your e-mail address above and click</br>" +
                "<br>the button below.</b>" +
                "<br><br>You will receive an e-mail with instructions.</br</br>"));

        btnResetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
