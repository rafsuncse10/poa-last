package com.bscheme.saddam.petsofasia.fragment.fragment_left_menu;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bscheme.petsofasia.R;
import com.bscheme.saddam.petsofasia.adapter.FavouritePetsAdapter;
import com.bscheme.saddam.petsofasia.model.PetIndividualInfo;
import com.bscheme.saddam.petsofasia.utils.ConnectionHelper;
import com.bscheme.saddam.petsofasia.utils.DividerItemDecoration;
import com.bscheme.saddam.petsofasia.utils.SharedDataSaveLoad;
import com.bscheme.saddam.petsofasia.utils.Urls;
import com.bscheme.saddam.petsofasia.widget.FullScreenDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */

public class PetsFavouriteFragment extends Fragment {

    RecyclerView mRecyclerView;
    List<PetIndividualInfo> mPetListInfoList;
    FavouritePetsAdapter mAdapter;

    public PetsFavouriteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pets_favourite, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_fav_petList);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(gridLayoutManager);
        DividerItemDecoration divider = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        mRecyclerView.addItemDecoration(divider);

        mPetListInfoList = new ArrayList<>();
        mAdapter = new FavouritePetsAdapter(getActivity(),mPetListInfoList);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new geFavouritePetListTask().execute();
    }

    void showDialog() {

        FullScreenDialog fragment = (FullScreenDialog) getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragment == null) {
            fragment = new FullScreenDialog();
            fragment.setCancelable(false);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(fragment, "dialog")
                    .commitAllowingStateLoss();

            // fragment.show(getSupportFragmentManager().beginTransaction(), LoadingDialogFragment.FRAGMENT_TAG);
        }

    }
    void closeDialog(){
        Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            // fragment.dismissAllowingStateLoss();
            getActivity().getSupportFragmentManager().beginTransaction().remove(prev).commitAllowingStateLoss();
//            prev.dismiss();
            Log.i("frag diag", "close");
        }
    }
    private class geFavouritePetListTask extends AsyncTask<String,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            showDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            ConnectionHelper ch = new ConnectionHelper();
            ch.createConnection(Urls.URL_COMMON, "POST");
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("action", "user_favorite_pet_list")
                    .appendQueryParameter("user_id", "" + SharedDataSaveLoad.load(getActivity(),getActivity().getResources().getString(R.string.shared_pref_user_email)));
            ch.addData(builder);

            return ch.getResponse();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s)) {

                Log.i("res",""+s);
                try {
                    JSONObject jObj = new JSONObject(s);
                    int success = jObj.getInt("success");
                    if (success == 0) {

                        closeDialog();
                        Toast.makeText(getActivity().getApplicationContext(), jObj.getString("success_status"), Toast.LENGTH_SHORT).show();
                    } else {
                        closeDialog();

                        JSONArray jsonArray = jObj.getJSONArray("pets_list");

                        for (int i = 0; i < jsonArray.length(); i++) {

                            PetIndividualInfo mPetIndividualInfo = new PetIndividualInfo();

                            mPetIndividualInfo.id = jsonArray.getJSONObject(i).getInt("id");
                            mPetIndividualInfo.title = jsonArray.getJSONObject(i).getString("title");
                            mPetIndividualInfo.about = jsonArray.getJSONObject(i).getString("about");
                            mPetIndividualInfo.featured_image = jsonArray.getJSONObject(i).getString("featured_image");
                            mPetIndividualInfo.location = jsonArray.getJSONObject(i).getString("location");
                            mPetIndividualInfo.shelter = jsonArray.getJSONObject(i).getString("shelter");
                            mPetIndividualInfo.type = jsonArray.getJSONObject(i).getString("type");
                            mPetIndividualInfo.gender = jsonArray.getJSONObject(i).getString("gender");
                            mPetIndividualInfo.age_range = jsonArray.getJSONObject(i).getString("age_range");
                            mPetIndividualInfo.size_type = jsonArray.getJSONObject(i).getString("size_type");
                            mPetIndividualInfo.breed_type = jsonArray.getJSONObject(i).getString("breed_type");
                            mPetIndividualInfo.vaccinated = jsonArray.getJSONObject(i).getString("vaccinated");
                            mPetIndividualInfo.contact_email = jsonArray.getJSONObject(i).getString("contact_email");
                            mPetIndividualInfo.contact_address = jsonArray.getJSONObject(i).getString("contact_address");
                            mPetIndividualInfo.contact_phone = jsonArray.getJSONObject(i).getString("contact_phone");
                            mPetIndividualInfo.contact_web = jsonArray.getJSONObject(i).getString("contact_web");
//                            mPetIndividualInfo.favourite = jsonArray.getJSONObject(i).getInt("favourite");

                            JSONArray jsonImages = jsonArray.getJSONObject(i).getJSONArray("extra_img");
                            List<String> imagesList = new ArrayList<>();
                            for (int k = 0; k < jsonImages.length(); k++) {

                                imagesList.add(jsonImages.getString(k));

                            }
                            mPetIndividualInfo.imageList.addAll(imagesList);

                            mPetListInfoList.add(mPetIndividualInfo);
                        }

                        mAdapter.notifyDataSetChanged();

                    }
                } catch (JSONException e) {

                    closeDialog();
                    Toast.makeText(getActivity(), "" + e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }

    }

}
